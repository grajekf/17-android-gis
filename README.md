## Reminder

### Opis

Aplikacja została stworzona w ramach przedmiotów *Aplikacje mobilne: Android* oraz *Aplikacje i usługi GIS*. Służy do tworzenia przypomnień, nie tylko czasowych, ale też przestrzennych. Umożliwia ustawienie powiadomień włączających się w momencie, w którym użytkownik znajdzie się w określonym miejscu, np. obok sklepu, w którym ma coś kupić.
Jest również opcja, aby wystarczająco wcześnie przypomnieć o wydarzeniu, uwzględniając czas dojazdu do lokalizacji tego wydarzenia.
### Architektura

Architektura aplikacji jest oparta o wzorzec MVP (Model-View-Presenter). Wykorzystywane są różne asynchroniczne źródła danych (baza danych, kalendarz Google, Google Maps/Directions API), skorzystano więc z reactive programming za pomocą biblioteki Rxjava.