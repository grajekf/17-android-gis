package pl.edu.pw.mini.grastudio.reminder;

import org.junit.Test;

import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.domain.GetEventForLocationUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.GetFutureEventsUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.GetLocationForTheNearestEventUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.GetNotAllDayEventsForDateUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.GetTheNearestEventUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.GetTodayAllDayEventsUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.GetTodayEventsUseCase;
import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.domain.GetLocationForEventUseCase;
import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.model.EventLocationLink;
import pl.edu.pw.mini.grastudio.reminder.model.EventLocationLink;
import pl.edu.pw.mini.grastudio.reminder.repository.EventsLocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.repository.IEventsLocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.repository.IRepository;
import pl.edu.pw.mini.grastudio.reminder.utils.DateUtils;
import rx.Observable;

import static org.junit.Assert.assertEquals;


public class UseCaseTests {

    private IRepository<Event> EventRepoWith(Event... events) {
        IRepository<Event> repo = new MockEventRepository();
        for(Event e : events) {
            repo.addOrUpdate(e);
        }
        return repo;
    }

    private IRepository<CustomerLocation> LocationRepoWith(CustomerLocation... locations) {
        IRepository<CustomerLocation> repo = new MockLocationRepository();
        for(CustomerLocation l : locations) {
            repo.addOrUpdate(l);
        }
        return repo;
    }

    private IEventsLocationsRepository EventLocationRepoWith(EventLocationLink... links) {
        IEventsLocationsRepository repo = new MockEventLocationsRepository();
        for(EventLocationLink l : links) {
            repo.addOrUpdate(l);
        }
        return repo;
    }

    @Test
    public void GetFutureEvents_ReturnsEventsFromFuture_OrAllDayEventsFromToday() throws Exception {
        //Arrange
        IRepository<Event> repo = EventRepoWith(
                new Event("test",
                        DateUtils.createDate(2017,5,29,18,0,0),
                        DateUtils.createDate(2017,5,29,18,50,0),
                        false,
                        Event.Priority.low),
                new Event("old",
                        DateUtils.createDate(2017,5,20,18,0,0),
                        DateUtils.createDate(2017,5,20,18,50,0),
                        false,
                        Event.Priority.low),
                new Event("allDay",
                        DateUtils.createDate(2017,5,29,2,0,0),
                        DateUtils.createDate(2017,5,29,2,0,0),
                        true,
                        Event.Priority.low)
                );
        GetFutureEventsUseCase useCase = new GetFutureEventsUseCase(repo,
                new MockDateService(DateUtils.createDate(2017,5,29,17,0,0)));
        //Act
        List<Event> events = TestUtils.resultFromObservable(useCase.execute());
        //Assert
        assertEquals(events.size(), 2);
        assertEquals(events.get(0).getName(), "allDay");
        assertEquals(events.get(1).getName(), "test");
    }

    @Test
    public void GetTodayEvents_ReturnsTodayEventsFromFuture_OrAllDayEventsFromToday() throws Exception {
        //Arrange
        IRepository<Event> repo = EventRepoWith(
                new Event("test",
                        DateUtils.createDate(2017,5,29,18,0,0),
                        DateUtils.createDate(2017,5,29,18,50,0),
                        false,
                        Event.Priority.low),
                new Event("old",
                        DateUtils.createDate(2017,5,20,18,0,0),
                        DateUtils.createDate(2017,5,20,18,50,0),
                        false,
                        Event.Priority.low),
                new Event("allDay",
                        DateUtils.createDate(2017,5,29,2,0,0),
                        DateUtils.createDate(2017,5,29,2,0,0),
                        true,
                        Event.Priority.low),
                new Event("tomorrow",
                        DateUtils.createDate(2017,5,30,18,0,0),
                        DateUtils.createDate(2017,5,30,18,50,0),
                        false,
                        Event.Priority.low)
        );
        GetTodayEventsUseCase useCase = new GetTodayEventsUseCase(repo, new MockDateService(DateUtils.createDate(2017,5,29,17,0,0)));
        //Act
        List<Event> events = TestUtils.resultFromObservable(useCase.execute());
        //Assert
        assertEquals(events.size(), 2);
        assertEquals(events.get(0).getName(), "allDay");
        assertEquals(events.get(1).getName(), "test");
    }

    @Test
    public void GetTodayAllDayEvents_AllDayEventsFromToday() throws Exception {
        //Arrange
        IRepository<Event> repo = EventRepoWith(
                new Event("test",
                        DateUtils.createDate(2017,5,29,18,0,0),
                        DateUtils.createDate(2017,5,29,18,50,0),
                        false,
                        Event.Priority.low),
                new Event("old",
                        DateUtils.createDate(2017,5,20,18,0,0),
                        DateUtils.createDate(2017,5,20,18,50,0),
                        false,
                        Event.Priority.low),
                new Event("allDay",
                        DateUtils.createDate(2017,5,29,2,0,0),
                        DateUtils.createDate(2017,5,29,2,0,0),
                        true,
                        Event.Priority.low),
                new Event("tomorrow",
                        DateUtils.createDate(2017,5,30,18,0,0),
                        DateUtils.createDate(2017,5,30,18,50,0),
                        false,
                        Event.Priority.low)
        );
        GetTodayAllDayEventsUseCase useCase = new GetTodayAllDayEventsUseCase(repo, new MockDateService(DateUtils.createDate(2017,5,29,17,0,0)));
        //Act
        List<Event> events = TestUtils.resultFromObservable(useCase.execute());
        //Assert
        assertEquals(events.size(), 1);
        assertEquals(events.get(0).getName(), "allDay");
    }

    @Test
    public void  GetTheNearestEventUseCase_ReturnsTheNearestEvent() throws Exception {
        //Arrange
        IRepository<Event> repo = EventRepoWith(
                new Event("test1",
                        DateUtils.createDate(2017,5,29,18,0,0),
                        DateUtils.createDate(2017,5,29,18,50,0),
                        false,
                        Event.Priority.low),
                new Event("test2",
                        DateUtils.createDate(2017,5,29,17,10,0),
                        DateUtils.createDate(2017,5,29,18,50,0),
                        false,
                        Event.Priority.low),
                new Event("test3",
                        DateUtils.createDate(2017,5,30,2,0,0),
                        DateUtils.createDate(2017,5,30,2,0,0),
                        true,
                        Event.Priority.low),
                new Event("test4",
                        DateUtils.createDate(2017,5,27,18,0,0),
                        DateUtils.createDate(2017,5,27,18,50,0),
                        false,
                        Event.Priority.low)
        );
        GetTheNearestEventUseCase useCase = new GetTheNearestEventUseCase(repo, new MockDateService(DateUtils.createDate(2017,5,29,17,0,0)));
        //Act
        Event event = TestUtils.resultFromObservable(useCase.execute());
        //Assert
        assertEquals(event.getName(), "test2");
    }

    @Test
    public void  GetLocationForTheNearestEventUseCase_ReturnsLocationsForTheNearestEvent() throws Exception {
        //Arrange
        IRepository<Event> repEvents = EventRepoWith(
                new Event(0,"test1",
                        DateUtils.createDate(2017,5,29,18,0,0),
                        DateUtils.createDate(2017,5,29,18,50,0),
                        false,
                        Event.Priority.low),
                new Event(1, "test2",
                        DateUtils.createDate(2017,5,29,17,10,0),
                        DateUtils.createDate(2017,5,29,18,50,0),
                        false,
                        Event.Priority.low),
                new Event(2, "test3",
                        DateUtils.createDate(2017,5,30,2,0,0),
                        DateUtils.createDate(2017,5,30,2,0,0),
                        true,
                        Event.Priority.low),
                new Event(3, "test4",
                        DateUtils.createDate(2017,5,30,18,0,0),
                        DateUtils.createDate(2017,5,30,18,50,0),
                        false,
                        Event.Priority.low)
        );

        IRepository<CustomerLocation> repLocations = LocationRepoWith(
                new CustomerLocation(0, "sklep", 200,200),
                new CustomerLocation(1, "bar", 100, 100),
                new CustomerLocation(2, "poczta", 300, 400),
                new CustomerLocation(3, "uczelnia", 500, 393)
        );

        IRepository<EventLocationLink> repLinks = EventLocationRepoWith(
                new EventLocationLink(0, 0, 0),
                new EventLocationLink(1, 1, 1),
                new EventLocationLink(2, 2, 2),
                new EventLocationLink(3, 3, 3)
        );
        GetLocationForTheNearestEventUseCase useCase = new GetLocationForTheNearestEventUseCase(repEvents, repLocations, repLinks, new MockDateService(DateUtils.createDate(2017,5,29,17,0,0)));
        //Act
        CustomerLocation location = TestUtils.resultFromObservable(useCase.execute());
        //Assert
        assertEquals(location.getId(), 1);
    }


    @Test
    public void GetEventForLocationUseCase_ReturnsCorrectEvents() throws Exception {
        //Arrange
        IRepository<Event> eventsRepo = EventRepoWith(
                new Event("test",
                        DateUtils.createDate(2017,5,29,18,0,0),
                        DateUtils.createDate(2017,5,29,18,50,0),
                        false,
                        Event.Priority.low),
                new Event("second",
                        DateUtils.createDate(2017,5,30,18,0,0),
                        DateUtils.createDate(2017,5,30,18,50,0),
                        false,
                        Event.Priority.low),
                new Event("allDay",
                        DateUtils.createDate(2017,5,29,2,0,0),
                        DateUtils.createDate(2017,5,29,2,0,0),
                        true,
                        Event.Priority.low)
        );
        CustomerLocation location1 = new CustomerLocation(0, "location1", 52, 21);
        CustomerLocation location2 = new CustomerLocation(1, "location1", 50, 15);
        IEventsLocationsRepository eventsLocationsRepo = EventLocationRepoWith(
                new EventLocationLink(-1, 0, 0),
                new EventLocationLink(-1, 1, 1),
                new EventLocationLink(-1, 2, 0)
        );
        GetEventForLocationUseCase useCase = new GetEventForLocationUseCase(eventsLocationsRepo, eventsRepo, location1);
        //Act
        List<Event> events = TestUtils.resultFromObservable(useCase.execute());
        //Assert
        assertEquals(events.size(), 2);
        assertEquals(events.get(0).getName(), "test");
        assertEquals(events.get(1).getName(), "allDay");
    }

    @Test
    public void GetLocationForEventUseCase_ReturnsLocationOfEvent() throws Exception {
        //Arrange
        IRepository<CustomerLocation> locationsRepo = LocationRepoWith(
            new CustomerLocation(-1, "location", 52, 21),
            new CustomerLocation(-1, "another", 50, 15)
        );
        IEventsLocationsRepository eventsLocationsRepo = EventLocationRepoWith(
                new EventLocationLink(-1, 0, 0),
                new EventLocationLink(-1, 1, 1)
        );
        Event event1 = new Event(0, "test",
                DateUtils.createDate(2017,5,29,18,0,0),
                DateUtils.createDate(2017,5,29,18,50,0),
                false,
                Event.Priority.low);
        Event event2 = new Event(1, "second",
                        DateUtils.createDate(2017,5,30,18,0,0),
                        DateUtils.createDate(2017,5,30,18,50,0),
                        false,
                        Event.Priority.low);
        GetLocationForEventUseCase useCase = new GetLocationForEventUseCase(locationsRepo, eventsLocationsRepo);
        useCase.setEvent(event1);
        //Act
        CustomerLocation location = TestUtils.resultFromObservable(useCase.execute());
        //Assert
        assertEquals(location.getName(), "location");
    }

//    @Test
//    public void GetNotAllDayEventsForDate_ReturnsNotAllDayEventsForSpecificDate() throws Exception {
//        //Arrange
//        IRepository<Event> repo = EventRepoWith(
//                new Event("test",
//                        DateUtils.createDate(2017,5,29,18,0,0),
//                        DateUtils.createDate(2017,5,29,18,50,0),
//                        false,
//                        Event.Priority.low),
//                new Event("old",
//                        DateUtils.createDate(2017,5,20,18,0,0),
//                        DateUtils.createDate(2017,5,20,18,50,0),
//                        false,
//                        Event.Priority.low),
//                new Event("allDay",
//                        DateUtils.createDate(2017,5,29,2,0,0),
//                        DateUtils.createDate(2017,5,29,2,0,0),
//                        true,
//                        Event.Priority.low),
//                new Event("tomorrow",
//                        DateUtils.createDate(2017,5,30,18,0,0),
//                        DateUtils.createDate(2017,5,30,18,50,0),
//                        false,
//                        Event.Priority.low)
//        );
//        GetNotAllDayEventsForDateUseCase useCase= new GetNotAllDayEventsForDateUseCase(repo);
//        useCase.setDate(DateUtils.createDate(2017,5,29,17,0,0));
//        //Act
//        List<Event> events = TestUtils.resultFromObservable(useCase.execute());
//        //Assert
//        assertEquals(1, events.size());
//        assertEquals("test", events.get(0).getName());
//    }

}
