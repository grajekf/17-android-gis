package pl.edu.pw.mini.grastudio.reminder;

import java.util.Date;

import pl.edu.pw.mini.grastudio.reminder.utils.IDateService;


public class MockDateService implements IDateService {
    private Date now;

    public MockDateService(Date now) {
        this.now = now;
    }

    @Override
    public Date Now() {
        return now;
    }

    @Override
    public boolean isToday(Date date) {
        return date.getYear() == now.getYear() && date.getMonth() == now.getMonth() && date.getDate() == now.getDate();
    }
}
