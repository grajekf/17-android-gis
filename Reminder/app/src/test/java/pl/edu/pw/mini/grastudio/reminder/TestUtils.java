package pl.edu.pw.mini.grastudio.reminder;

import rx.Observable;
import rx.observers.TestSubscriber;

public class TestUtils {
    public static <T> T resultFromObservable(Observable<T> observable) {
        TestSubscriber<T> testSubscriber = new TestSubscriber<>();
        observable.subscribe(testSubscriber);
        return testSubscriber.getOnNextEvents().get(0);
    }
}
