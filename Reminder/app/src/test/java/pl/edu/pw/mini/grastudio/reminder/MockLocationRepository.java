package pl.edu.pw.mini.grastudio.reminder;

import java.util.ArrayList;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.repository.IRepository;
import rx.Observable;


public class MockLocationRepository implements IRepository<CustomerLocation> {
    List<CustomerLocation> customerLocations = new ArrayList<>();
    long lastId = 0;

    @Override
    public Observable<CustomerLocation> getById(long id) {
        for(int i = 0; i < customerLocations.size(); i++)
            if(customerLocations.get(i).getId() == id)
                return Observable.just(customerLocations.get(i));
        return null;
    }

    @Override
    public Observable<List<CustomerLocation>> getAll() {
        return Observable.just(customerLocations);
    }

    @Override
    public Observable<Long> addOrUpdate(CustomerLocation item) {
        //only adding
        long oldId = lastId++;
        item.setId(oldId);
        customerLocations.add(item);
        return Observable.just(oldId);
    }

    @Override
    public Observable<Integer> delete(long id) {
        return null;
    }
}
