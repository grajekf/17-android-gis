package pl.edu.pw.mini.grastudio.reminder.view;


import java.util.HashMap;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.Event;

public interface IEventListView extends View {
    void setGroupedEvents(HashMap<String, List<Event>> groupedEvents);

    void returnToLastScreen();
}
