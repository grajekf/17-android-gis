package pl.edu.pw.mini.grastudio.reminder;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.gson.Gson;

import java.util.Date;
import java.util.concurrent.RunnableFuture;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.edu.pw.mini.grastudio.reminder.database.DatabaseHandler;
import pl.edu.pw.mini.grastudio.reminder.dialogs.DatePickerFragment;
import pl.edu.pw.mini.grastudio.reminder.dialogs.TimePickerFragment;
import pl.edu.pw.mini.grastudio.reminder.domain.AddOrModifyEventLocationLinkUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.AddOrModifyEventUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.AddOrModifyLocationUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.GetLinkForEventUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.GetLocationForEventUseCase;
import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.presenter.AddEventPresenter;
import pl.edu.pw.mini.grastudio.reminder.presenter.IAddEventPresenter;
import pl.edu.pw.mini.grastudio.reminder.repository.EventRepository;
import pl.edu.pw.mini.grastudio.reminder.repository.EventsLocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.repository.LocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.utils.EnumUtils;
import pl.edu.pw.mini.grastudio.reminder.view.IAddEventView;

public class AddEventActivity extends AppCompatActivity implements IAddEventView,
        SelectTimeFragment.OnClickTimeListener,
        SelectLocationFragment.OnLocationSelectedListener,
        TabLayout.OnTabSelectedListener{
    @BindView(R.id.titleEditText)
    public EditText nameEditText;
    @BindView(R.id.eventAllDaySwitch)
    public Switch isAllDaySwitch;
    @BindView(R.id.startDateTextView)
    public TextView startDateTextView;
    @BindView(R.id.endDateTextView)
    public TextView endDateTextView;
    @BindView(R.id.locationTextView)
    public TextView locationTextView;
    @BindView(R.id.spinnerPriority)
    public Spinner prioritySpinner;
    @BindView(R.id.tab_layout)
    public TabLayout tabs;
    @BindView(R.id.distanceEditText)
    public EditText distanceEditText;


    private IAddEventPresenter presenter;

    private SelectTimeFragment currentSelectTimeFragment;
    private SelectDurationFragment currentSelectDurationFragment;

    public static final String ARG_EVENT = "event";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);
        String data = getIntent().getStringExtra(ARG_EVENT);
        if(data == null)
            initializePresenter(null); //we are adding a new event
        else
            initializePresenter(new Gson().fromJson(data, Event.class));


        isAllDaySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                presenter.onAllDayCheckChanged(isChecked);
            }
        });

        initializeSpinner();



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.action_add) {
            if(nameEditText.getText().toString().length() == 0)
                nameEditText.setError(getResources().getString(R.string.name_empty));
            else
                presenter.addOrUpdateEvent(nameEditText.getText().toString(),
                        startDateTextView.getText().toString(),
                        endDateTextView.getText().toString(),
                        currentSelectTimeFragment.getStartTime(),
                        currentSelectTimeFragment.getEndTime(),
                        isAllDaySwitch.isChecked(),
                        currentSelectDurationFragment.getDurationHours(),
                        currentSelectDurationFragment.getDurationMinutes(),
                        prioritySpinner.getSelectedItemPosition(),
                        distanceEditText.getText().toString(),
                        tabs.getSelectedTabPosition());
        }
        if(id == R.id.action_back) {
            presenter.cancelEditing();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void loadDateAndTime(String date, String time) {
        startDateTextView.setText(date);
//        startTimeTextView.setText(time);
        endDateTextView.setText(date);
//        endTimeTextView.setText(time);

        currentSelectTimeFragment = SelectTimeFragment.newInstance(time, time);
        currentSelectDurationFragment = SelectDurationFragment.newInstance(0, 0);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.add_grid, currentSelectTimeFragment);
        transaction.commit();
        currentSelectTimeFragment.onAttach(this);
    }

    @Override
    public void showStartDateDialog(int year, int month, int dayOfMonth) {
        DialogFragment dateFrag = DatePickerFragment.newInstance(year, month, dayOfMonth, new DatePickerFragment.OnDatePickedListener() {
            @Override
            public void onDatePicked(int year, int month, int dayOfMonth) {
                presenter.setNewStartDate(year, month, dayOfMonth);
            }
        });
        dateFrag.show(getFragmentManager(), "datePicker");
    }

    @Override
    public void setStartDate(String date) {
        startDateTextView.setText(date);
    }

    @Override
    public void showEndDateDialog(int year, int month, int dayOfMonth) {
        DialogFragment dateFrag = DatePickerFragment.newInstance(year, month, dayOfMonth, new DatePickerFragment.OnDatePickedListener() {
            @Override
            public void onDatePicked(int year, int month, int dayOfMonth) {
                presenter.setNewEndDate(year, month, dayOfMonth);
            }
        });
        dateFrag.show(getFragmentManager(), "datePicker");
    }

    @Override
    public void setEndDate(String date) {
        endDateTextView.setText(date);
    }

    @Override
    public void showStartTimeDialog(int hours, int minutes) {
        DialogFragment timeFrag = TimePickerFragment.newInstance(hours, minutes, new TimePickerFragment.OnTimePickListener() {
            @Override
            public void OnTimePick(int hour, int minute) {
                presenter.setNewStartTime(hour, minute);
            }
        });
        timeFrag.show(getFragmentManager(), "timeDialog");
    }

    @Override
    public void showEndTimeDialog(int hours, int minutes) {
        DialogFragment timeFrag = TimePickerFragment.newInstance(hours, minutes, new TimePickerFragment.OnTimePickListener() {
            @Override
            public void OnTimePick(int hour, int minute) {
                presenter.setNewEndTime(hour, minute);
            }
        });
        timeFrag.show(getFragmentManager(), "timeDialog");
    }

    @Override
    public void setStartTime(String time) {
//        startTimeTextView.setText(time);
        currentSelectTimeFragment.setStartTime(time);
    }

    @Override
    public void setEndTime(String time) {
//        endTimeTextView.setText(time);
        currentSelectTimeFragment.setEndTime(time);
    }

    @Override
    public void onStartTimeClick(String startDate) {
        presenter.pickNewStartTime(currentSelectTimeFragment.getStartTime());
    }

    @Override
    public void onEndTimeClick(String endDate) {
        presenter.pickNewEndTime(currentSelectTimeFragment.getEndTime());
    }

    @Override
    public void returnToLastScreen() {
        finish();
    }

    @Override
    public void returnToLastScreenWithResult(String serializedResult) {
        Intent data = new Intent();
        data.putExtra(ARG_EVENT, serializedResult);
        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public void startPlacePickerDialog() {
//        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
//        try {
//            Intent intent = builder.build(this);
//            startActivityForResult(intent, RequestCode.PICK_LOCATION);
//        } catch (GooglePlayServicesRepairableException e) {
//            e.printStackTrace();
//        } catch (GooglePlayServicesNotAvailableException e) {
//            e.printStackTrace();
//        }
        SelectLocationFragment newFragment = SelectLocationFragment.newInstance();
        newFragment.attachListener(this);
        newFragment.show(getSupportFragmentManager().beginTransaction(), "dialog");
    }

    @Override
    public void setLocationText(String s) {
        locationTextView.setText(s);
    }

    @Override
    public void showDurationFragment(int hours, int minutes) {
        currentSelectDurationFragment = SelectDurationFragment.newInstance(hours, minutes);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.remove(currentSelectTimeFragment);
        transaction.replace(R.id.add_grid, currentSelectDurationFragment);
        transaction.commit();
    }

    @Override
    public void showTimeFragment(String startTime, String endTime) {
        currentSelectTimeFragment = SelectTimeFragment.newInstance(startTime, endTime);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.remove(currentSelectDurationFragment);
        transaction.replace(R.id.add_grid, currentSelectTimeFragment);
        transaction.commit();
    }

    @Override
    public void reloadDurationFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.add_grid, currentSelectDurationFragment);
        transaction.commit();
    }

    @Override
    public void reloadTimeFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.add_grid, currentSelectTimeFragment);
        transaction.commit();
    }

    @Override
    public void setDuration(int durationHours, int durationMinutes) {
        currentSelectDurationFragment.setDurationHours(durationHours);
        currentSelectDurationFragment.setDurationMinutes(durationMinutes);
    }

    @Override
    public void setTitle(String title) {
        nameEditText.setText(title);
    }

    @Override
    public void setAllDay(boolean allDay) {
        isAllDaySwitch.setChecked(allDay);
    }

    @Override
    public void setSelectedPriority(final int index) {
        prioritySpinner.post(new Runnable() {
            @Override
            public void run() {
                prioritySpinner.setSelection(index);
            }
        });
    }

    @Override
    public void setTransport(int transportInt) {
        tabs.getTabAt(transportInt).select();
    }

    @Override
    public void setDistance(double reminderDistanceMeters) {
        distanceEditText.setText(String.valueOf(reminderDistanceMeters));
    }

    @Override
    public void onLocationSelected(long id, String name, String address, String latLng, double lat, double lng, boolean isFav) {
        presenter.onPlaceSelected(id, name, address, latLng, lat, lng, isFav);
    }

//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == RequestCode.PICK_LOCATION) {
//            if (resultCode == RESULT_OK) {
//                Place place = PlacePicker.getPlace(data, this);
//                presenter.onPlaceSelected(place.getName().toString(), place.getAddress().toString(),
//                        place.getLatLng().toString(), place.getLatLng().latitude, place.getLatLng().longitude );
//            }
//        }
//    }

    private void initializePresenter(Event event) {
        presenter = new AddEventPresenter(new AddOrModifyEventUseCase(new EventRepository(this, new DatabaseHandler(this))),
                new AddOrModifyEventLocationLinkUseCase(new EventsLocationsRepository(new DatabaseHandler(this))),
                new AddOrModifyLocationUseCase(new LocationsRepository(new DatabaseHandler(this))),
                new GetLocationForEventUseCase(new LocationsRepository(new DatabaseHandler(this)),
                        new EventsLocationsRepository(new DatabaseHandler(this))),
                new GetLinkForEventUseCase(new EventsLocationsRepository(new DatabaseHandler(this))));
        presenter.attachView(this);
        presenter.setEvent(event);
    }

    private  void initializeSpinner() {
        String[] priorityNames = new String[Event.Priority.values().length];
        for(int i = 0; i < Event.Priority.values().length; i++) {
            priorityNames[i] = EnumUtils.ToLocalizedString(Event.Priority.values()[i], this);
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, priorityNames);
        prioritySpinner.setAdapter(adapter);

        presenter.initPriority(priorityNames);
    }

    public void onStartTimeClick(View v) {
        currentSelectTimeFragment.onStartTimeClick(v);
    }

    public void onEndTimeClick(View v) {
        currentSelectTimeFragment.onEndTimeClick(v);
    }

    public void onStartDateClick(View v){
        presenter.pickNewStartDate(startDateTextView.getText().toString());
    }

    public void onEndDateClick(View v){
        presenter.pickNewEndDate(endDateTextView.getText().toString());
    }

    public void onLocationClick(View v) {
        presenter.pickPlace();
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
