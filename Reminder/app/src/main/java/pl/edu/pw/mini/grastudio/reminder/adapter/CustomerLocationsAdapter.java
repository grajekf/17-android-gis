package pl.edu.pw.mini.grastudio.reminder.adapter;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.R;
import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;

/**
 * Created by Kamil on 03.05.2017.
 */

public class CustomerLocationsAdapter  extends RecyclerView.Adapter<CustomerLocationsAdapter.MyViewHolder> implements IDynamicAdapter {

    private List<CustomerLocation> locationsList;
    private OnLocationDeletedListener onLocationDeletedListener;

    @Override
    public void onDelete(int pos) {
        if(onLocationDeletedListener != null)
            onLocationDeletedListener.onDeleted(locationsList.get(pos));
        locationsList.remove(pos);
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView image;
        // public ImageButton delete;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            image = (ImageView) itemView.findViewById(R.id.image_view);
            //delete = (ImageButton) view.findViewById(R.id.deleteButton);
        }


    }//inner class


    public CustomerLocationsAdapter(List<CustomerLocation> locationsList) {
        this.locationsList = locationsList;
    }

    public CustomerLocationsAdapter(){
        this.locationsList = new ArrayList<>();
    }

    public void replaceData(List<CustomerLocation> locationsList){
        this.locationsList = locationsList;

        notifyDataSetChanged();
    }

    public void addLocation(CustomerLocation customerLocation){
        locationsList.add(customerLocation);
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.location_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        CustomerLocation shoppingItem = locationsList.get(position);
        holder.name.setText(shoppingItem.getName());

        if(position == 0 ||
                locationsList.get(position - 1).getName().toUpperCase().charAt(0) !=
                        shoppingItem.getName().toUpperCase().charAt(0)) {
            ColorGenerator generator = ColorGenerator.MATERIAL;
            int color = generator.getColor(shoppingItem);
            String letter = String.valueOf(shoppingItem.getName().toUpperCase().charAt(0));
            TextDrawable drawable = TextDrawable.builder().buildRound(letter, color);
            holder.image.setImageDrawable(drawable);
        }
        else {
            holder.image.setImageDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

    }

    @Override
    public int getItemCount() {
        return locationsList.size();
    }

    public void attachListener(OnLocationDeletedListener onLocationDeletedListener) {
        this.onLocationDeletedListener = onLocationDeletedListener;
    }

    public interface OnLocationDeletedListener {
        void onDeleted(CustomerLocation location);
    }
}