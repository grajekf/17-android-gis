package pl.edu.pw.mini.grastudio.reminder.view;

import java.util.HashMap;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.model.LocalizedEvent;


public interface IDayScheduleView extends View {
    void setEvents(List<LocalizedEvent> localizedEvents);
    void setGroupedEvents(List<List<LocalizedEvent>> eventsDividedInRows);
}
