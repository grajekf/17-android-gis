package pl.edu.pw.mini.grastudio.reminder.model;

/**
 * Created by Kamil on 22.04.2017.
 */

public class EventLocationLink extends Entity {
    private long eventId;
    private long locationId;

    public long getLocationId() {
        return locationId;
    }

    public EventLocationLink(long id, long eventId, long locationId) {
        super(id);
        this.eventId = eventId;
        this.locationId = locationId;
    }

    public void setLocationId(long locationId) {
        this.locationId = locationId;
    }

    public long getEventId() {

        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }
}
