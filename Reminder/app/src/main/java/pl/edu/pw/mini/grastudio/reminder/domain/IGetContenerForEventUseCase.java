package pl.edu.pw.mini.grastudio.reminder.domain;

import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.model.EventLocationContener;

public interface IGetContenerForEventUseCase extends IUseCase<EventLocationContener> {
    void setEvent(Event event);
}
