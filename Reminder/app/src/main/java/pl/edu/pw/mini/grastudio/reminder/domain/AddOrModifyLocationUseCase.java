package pl.edu.pw.mini.grastudio.reminder.domain;

import android.location.Location;

import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.repository.EventRepository;
import pl.edu.pw.mini.grastudio.reminder.repository.LocationsRepository;
import rx.Observable;

/**
 * Created by Kamil on 22.04.2017.
 */

public class AddOrModifyLocationUseCase implements IAddOrModifyLocationUseCase {

    private LocationsRepository repository;
    private CustomerLocation locationToAdd;

    public AddOrModifyLocationUseCase(LocationsRepository repository) {
        this.repository = repository;
    }

    @Override
    public void setLocationToAdd(CustomerLocation locationToAdd) {
        this.locationToAdd = locationToAdd;
    }

    @Override
    public Observable<Long> execute() {
        if(locationToAdd == null)
            return Observable.just(-1L);
        return repository.addOrUpdate(locationToAdd);
    }
}
