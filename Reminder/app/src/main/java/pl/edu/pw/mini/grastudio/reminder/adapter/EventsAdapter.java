package pl.edu.pw.mini.grastudio.reminder.adapter;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.edu.pw.mini.grastudio.reminder.EventDetailsFragment;
import pl.edu.pw.mini.grastudio.reminder.R;
import pl.edu.pw.mini.grastudio.reminder.domain.DeleteEventUseCase;
import pl.edu.pw.mini.grastudio.reminder.model.Event;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventViewHolder> implements IDynamicAdapter {

    private List<Event> events;
    private Context context;
    private OnEventDeletedListener onEventDeletedListener;
    private OnClickedListener onClickedListener;


    public EventsAdapter(Context context, List<Event> events, OnClickedListener onClickedListener) {
        this.context = context;
        this.events = events;
        this.onClickedListener = onClickedListener;
    }

    public void attachListener(OnEventDeletedListener onEventDeletedListener) {
        this.onEventDeletedListener = onEventDeletedListener;
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        //get view from layout
        View itemView = inflater.inflate(R.layout.event_row_item, parent, false);
        return new EventViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        EventViewHolder viewHolder = (EventViewHolder)holder;
        SimpleDateFormat durationFormat = new SimpleDateFormat("H:mm");
        Event currentEvent = events.get(position);

        viewHolder.titleTextView.setText(currentEvent.getName());
        if(currentEvent.isAllDay())
            viewHolder.durationTextView.setText(context.getResources().getString(R.string.event_all_day));
        else
            viewHolder.durationTextView.setText(durationFormat.format(currentEvent.getStartDate()));

        viewHolder.priorityView.setBackgroundColor(currentEvent.getColor());

        if(position == getItemCount() - 1) //hide divider if this is the last child
            viewHolder.divider.setVisibility(View.GONE);
    }

    @Override
    public void onDelete(int pos) {
        if(onEventDeletedListener != null)
            onEventDeletedListener.onDeleted(events.get(pos));
        events.remove(pos);
        notifyItemRemoved(pos);
    }

    public class EventViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.eventTitleTextView)
        public TextView titleTextView;
        @BindView(R.id.eventDurationTextView)
        public TextView durationTextView;
        @BindView(R.id.priorityView)
        public View priorityView;
        @BindView(R.id.divider)
        public View divider;

        public EventViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if(position != RecyclerView.NO_POSITION) { //an item was selected
                Event event = events.get(position);
                onClickedListener.onClicked(event);
//                String oldTitle = ((Activity)context).getTitle().toString();
//                EventDetailsFragment frag = EventDetailsFragment.newInstance(event, oldTitle);
//                FragmentTransaction transaction = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
////                transaction.setCustomAnimations(R.anim.anim_in, R.anim.anim_out);
//                transaction.replace(((View)(v.getParent().getParent())).getId(), frag, EventDetailsFragment.TAG).addToBackStack(EventDetailsFragment.TAG);
//                transaction.commit();
            }
        }
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public interface OnEventDeletedListener {
        void onDeleted(Event event);
    }

    public interface OnClickedListener {
        void onClicked(Event event);
    }
}
