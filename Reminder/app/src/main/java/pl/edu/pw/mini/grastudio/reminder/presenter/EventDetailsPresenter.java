package pl.edu.pw.mini.grastudio.reminder.presenter;


import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;

import pl.edu.pw.mini.grastudio.reminder.domain.IGetLocationForEventUseCase;
import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.view.IEventDetailsView;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class EventDetailsPresenter implements IEventDetailsPresenter {

    private IEventDetailsView view;
    private Event event;
    private CustomerLocation location;
    private Subscription getLocationSubscription;
    private IGetLocationForEventUseCase locationForEventUseCase;

    public EventDetailsPresenter(IGetLocationForEventUseCase locationForEventUseCase, Event event) {
        this.event = event;
        this.locationForEventUseCase = locationForEventUseCase;
    }


    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        //stop getting location
        if(getLocationSubscription != null && !getLocationSubscription.isUnsubscribed()) {
            getLocationSubscription.unsubscribe();
        }
    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(IEventDetailsView view) {
        this.view = view;
    }


    @Override
    public void setEvent(Event event) {
        this.event = event;
    }

    @Override
    public void onCreated() {
        if(event != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd.MM.yyyy");
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
            view.setTitle(event.getName());
            view.setDate(dateFormat.format(event.getStartDate()));
            if(!event.isAllDay())
                view.setTime(timeFormat.format(event.getStartDate()) + " - " + timeFormat.format(event.getEndDate()));
            else
                view.setDuration(event.getDurationHours(), event.getDurationMinutes());
            getLocation();
        }
    }

    @Override
    public void onEdit() {
        String serializedEvent = new Gson().toJson(event);
        view.navigateToEditScreen(serializedEvent);
    }

    @Override
    public void onEditedEvent(String serializedData) {
        event = new Gson().fromJson(serializedData, Event.class);
        onCreated();
    }

    @Override
    public LatLng getLatLngLocation() {
        if(location == null)
            return null;
        return new LatLng(location.getLat(), location.getLng());
    }

    private void getLocation() {
        locationForEventUseCase.setEvent(event);
        getLocationSubscription = locationForEventUseCase.execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(new Func1<Throwable, CustomerLocation>() {
                    @Override
                    public CustomerLocation call(Throwable throwable) {
                        throwable.printStackTrace();
                        return null;
                    }
                })
                .subscribe(new Action1<CustomerLocation>() {
                    @Override
                    public void call(CustomerLocation customerLocation) {
                        if(customerLocation != null) {
                            location = customerLocation;
                            String displayName = location.getName();
                            if (displayName == null || displayName.length() == 0)
                                displayName = Double.toString(location.getLat()) + " " + Double.toString(location.getLng());
                            view.setLocation(displayName);
                        }
                    }
                });
    }
}
