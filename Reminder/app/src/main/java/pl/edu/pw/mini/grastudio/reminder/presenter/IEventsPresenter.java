package pl.edu.pw.mini.grastudio.reminder.presenter;


import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.view.IEventsView;

public interface IEventsPresenter extends IPresenter<IEventsView> {

    void floatingActionButtonOnClick();
    void onEventDeleted(Event event);
    void onEventAdded(String serializedEvent);
}
