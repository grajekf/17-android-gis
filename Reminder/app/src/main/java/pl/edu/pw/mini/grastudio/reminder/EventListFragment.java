package pl.edu.pw.mini.grastudio.reminder;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.edu.pw.mini.grastudio.reminder.adapter.DateAdapter;
import pl.edu.pw.mini.grastudio.reminder.adapter.EventsAdapter;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.presenter.EventListPresenter;
import pl.edu.pw.mini.grastudio.reminder.presenter.IEventListPresenter;
import pl.edu.pw.mini.grastudio.reminder.view.IEventListView;


public class EventListFragment extends Fragment implements IEventListView, EventsAdapter.OnClickedListener {
    private static final String ARG_EVENT_LIST = "event_list";

    private IEventListPresenter presenter;
    private List<Event> events;

    private DateAdapter dateAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @BindView(R.id.datesRecycleView)
    public RecyclerView datesRecyclerView;

    public static final String TAG = "eventListFragment";

    public EventListFragment() {
        // Required empty public constructor
    }

    public static EventListFragment newInstance(List<Event> events) {
        EventListFragment fragment = new EventListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_EVENT_LIST, new Gson().toJson(events));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            String eventsString = getArguments().getString(ARG_EVENT_LIST);
            Type listType = new TypeToken<ArrayList<Event>>(){}.getType();
            events = new Gson().fromJson(eventsString, listType);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_event_list, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.map_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_back_list) {
            presenter.onMenuBack();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        ButterKnife.bind(this, getView());

        initializeRecycleView();
        initializePresenter();

        getView().setBackgroundColor(Color.WHITE);
        getView().setClickable(true);

    }

    @Override
    public void onStart(){
        super.onStart();

        ((AppCompatActivity) getActivity()).findViewById(R.id.tab_layout).setVisibility(View.GONE);
        ((AppCompatActivity) getActivity()).findViewById(R.id.fab).setVisibility(View.GONE);
        getActivity().findViewById(R.id.toolbar).setVisibility(View.VISIBLE);

        getActivity().setTitle("");

        presenter.onStart();
    }

    @Override
    public void setGroupedEvents(HashMap<String, List<Event>> groupedEvents) {
        dateAdapter.replaceData(groupedEvents);
    }

    @Override
    public void returnToLastScreen() {
        getActivity().findViewById(R.id.tab_layout).setVisibility(View.VISIBLE);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.remove(this);
        ft.commit();
    }

    private void initializePresenter() {
        presenter = new EventListPresenter();
        presenter.attachView(this);
        presenter.setEvents(events);
    }

    private void initializeRecycleView() {
        //this improves performance
        datesRecyclerView.setHasFixedSize(true);
        datesRecyclerView.setNestedScrollingEnabled(false);
        //linear manager -> a list!
        layoutManager = new LinearLayoutManager(getContext());
        datesRecyclerView.setLayoutManager(layoutManager);
        dateAdapter = new DateAdapter(getContext(), this);
//        dateAdapter.attachListener(this);
        datesRecyclerView.setAdapter(dateAdapter);
    }

    @Override
    public void onStop() {
        presenter.onStop();
        ((AppCompatActivity) getActivity()).findViewById(R.id.tab_layout).setVisibility(View.VISIBLE);
        ((AppCompatActivity) getActivity()).findViewById(R.id.fab).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.toolbar).setVisibility(View.GONE);
        super.onStop();
    }

    @Override
    public void onClicked(Event event) {
        String oldTitle = getActivity().getTitle().toString();
        EventDetailsFragment frag = EventDetailsFragment.newInstance(event, oldTitle, false);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
//                transaction.setCustomAnimations(R.anim.anim_in, R.anim.anim_out);
        transaction.add(R.id.content_map, frag, EventDetailsFragment.TAG).addToBackStack(EventDetailsFragment.TAG);
        transaction.commit();
    }
}
