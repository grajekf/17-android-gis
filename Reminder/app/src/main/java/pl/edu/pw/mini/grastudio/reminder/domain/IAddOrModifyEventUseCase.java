package pl.edu.pw.mini.grastudio.reminder.domain;

import pl.edu.pw.mini.grastudio.reminder.model.Event;

public interface IAddOrModifyEventUseCase extends IUseCase<Long> {
    void setEventToAdd(Event eventToAdd);
}
