package pl.edu.pw.mini.grastudio.reminder.utils;


import android.text.format.*;

import java.util.Date;

public class DateService implements IDateService {
    @Override
    public Date Now() {
        return new Date();
    }

    @Override
    public boolean isToday(Date date) {
        return android.text.format.DateUtils.isToday(date.getTime());
    }
}
