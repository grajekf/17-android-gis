package pl.edu.pw.mini.grastudio.reminder;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.edu.pw.mini.grastudio.reminder.adapter.SelectLocationAdapter;
import pl.edu.pw.mini.grastudio.reminder.database.DatabaseHandler;
import pl.edu.pw.mini.grastudio.reminder.domain.GetFavoriteLocationsUseCase;
import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.presenter.ISelectLocationPresenter;
import pl.edu.pw.mini.grastudio.reminder.presenter.SelectLocationPresenter;
import pl.edu.pw.mini.grastudio.reminder.repository.LocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.view.ISelectLocationView;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Kamil on 05.05.2017.
 */

public class TimeFilterFragment  extends DialogFragment  {

    @BindView(R.id.startTimeTextView)
    TextView startTimeTextView;
    @BindView(R.id.endTimeTextView)
    TextView endTimeTextView;
    @BindView(R.id.rangeSeekbar)
    CrystalRangeSeekbar rangeSeekBar;
    @BindView(R.id.okButton)
    Button okButton;

    TimeFilterFragment.TimeFilterListener  timeFilterListener;

    private SelectTimeFragment.OnClickTimeListener listener;

    public TimeFilterFragment() {}
        // Required empty public constructor

    Calendar start;
    Calendar end;

        class RequestCode {
            static final int PICK_LOCATION = 1;
        }


    static TimeFilterFragment newInstance() {
        TimeFilterFragment f = new TimeFilterFragment();

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ButterKnife.bind(this, getView());

        initializeCalendar();
        initializeRangeSeekBar();
        initializeButton();

    }

    private void initializeButton() {
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishWithResult();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View v = inflater.inflate(R.layout.fragment_time_filter, container, false);

        return v;
    }



    private void finishWithResult() {
        timeFilterListener.SetDates(start.getTime(), end.getTime());
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    private void initializeCalendar(){
        start = Calendar.getInstance();
        start.setTime(Calendar.getInstance().getTime());

        end = Calendar.getInstance();
        end.setTime(Calendar.getInstance().getTime());
    }

    private void initializeRangeSeekBar(){
        rangeSeekBar.setMinValue(0);
        rangeSeekBar.setMaxValue(10);
        rangeSeekBar.setSteps(1);
        rangeSeekBar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                start.setTime(Calendar.getInstance().getTime());
                start.add(Calendar.DATE, minValue.intValue());

                end.setTime(Calendar.getInstance().getTime());
                end.add(Calendar.DATE, maxValue.intValue());

                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
                startTimeTextView.setText(format1.format(start.getTime()));
                endTimeTextView.setText(format1.format(end.getTime()));

//                startTimeTextView.setText(String.valueOf(minValue.intValue()));
//                endTimeTextView.setText(String.valueOf(maxValue.intValue()));
            }
        });
    }

    public void attachListener(TimeFilterFragment.TimeFilterListener timeFilterListener) {
        this.timeFilterListener = timeFilterListener;
    }

    public interface TimeFilterListener {
        void SetDates(Date startDate, Date endDate);
    }
}
