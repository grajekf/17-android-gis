package pl.edu.pw.mini.grastudio.reminder.domain;

import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.model.EventLocationLink;

public interface IGetLinkForEventUseCase extends IUseCase<EventLocationLink> {
    void setEvent(Event event);
}
