package pl.edu.pw.mini.grastudio.reminder.presenter;

import com.google.gson.Gson;

import pl.edu.pw.mini.grastudio.reminder.domain.IAddOrModifyLocationUseCase;
import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.view.IAddFavouriteView;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;


public class AddFavoritePresenter implements IAddFavoritePresenter {

    IAddFavouriteView view;
    private CustomerLocation location;
    private IAddOrModifyLocationUseCase addOrModifyLocationUseCase;
    private Subscription addOrModifyLocationSubscription;

    public AddFavoritePresenter(IAddOrModifyLocationUseCase addOrModifyLocationUseCase) {
        this.addOrModifyLocationUseCase = addOrModifyLocationUseCase;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if(addOrModifyLocationSubscription != null && !addOrModifyLocationSubscription.isUnsubscribed())
            addOrModifyLocationSubscription.unsubscribe();
    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(IAddFavouriteView view) {
        this.view = view;
    }

    @Override
    public void pickPlace() {
        view.startPlacePickerDialog();
    }

    @Override
    public void onPlaceSelected(String name, String address, String latLng, double latitude, double longitude) {
        String text = name;
        if (address != "")
            text = text + ", " + address;
        if (text.length() == 0)
            text = latLng;
        view.setLocationText(text);

        location = new CustomerLocation(-1, text, latitude, longitude, true);
    }

    @Override
    public void cancelEditing() {
        view.returnToLastScreen();
    }

    @Override
    public void addOrUpdateFavorite(String name) {
        location.setName(name);
        addOrModifyLocationUseCase.setLocationToAdd(location);
        addOrModifyLocationSubscription = addOrModifyLocationUseCase.execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(new Func1<Throwable, Long>() {
                    @Override
                    public Long call(Throwable throwable) {
                        throwable.printStackTrace();
                        return null;
                    }
                })
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long id) {
                        view.returnToLastScreenWithResult(new Gson().toJson(location));
                    }
                });
    }

    @Override
    public boolean hasLocation() {
        return location != null;
    }
}
