package pl.edu.pw.mini.grastudio.reminder.domain;

import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;

public interface IGetEventForLocationUseCase extends IUseCase<List<Event>> {
    void setLocation(CustomerLocation location);

    void setId(long id);
}
