package pl.edu.pw.mini.grastudio.reminder.domain;

import java.util.Date;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.Event;

/**
 * Created by Kamil on 07.06.2017.
 */

public interface IGetNotAllDayEventsForDateUseCase extends  IUseCase<List<Event>> {
    void setDate(Date date);
}
