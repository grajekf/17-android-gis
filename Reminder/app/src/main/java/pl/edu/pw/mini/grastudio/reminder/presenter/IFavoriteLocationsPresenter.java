package pl.edu.pw.mini.grastudio.reminder.presenter;

import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.view.IFavoriteView;

/**
 * Created by Kamil on 03.05.2017.
 */

public interface IFavoriteLocationsPresenter extends IPresenter<IFavoriteView>  {
    void chooseNewFavoriteLocation();
    void insertNewFavoriteLocation(String name,double lat, double lng);
    void onLocationAdded(String serializedData);
    void onLocationDeleted(CustomerLocation location);
}
