package pl.edu.pw.mini.grastudio.reminder.presenter;


import android.util.Log;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.domain.IDeleteEventUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.IUseCase;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.view.IEventsView;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class EventsPresenter implements IEventsPresenter {

    private IEventsView eventsView;
    private Subscription getEventsSubscription;
    private Subscription deleteEventSubscription;
    private IUseCase<List<Event>> getEventsUseCase;
    private IDeleteEventUseCase deleteEventUseCase;
    private HashMap<String, List<Event>> groupedEvents;

    public EventsPresenter(IUseCase<List<Event>> getEventsUseCase, IDeleteEventUseCase deleteEventUseCase) {
        this.getEventsUseCase = getEventsUseCase;
        this.deleteEventUseCase = deleteEventUseCase;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {
        getEvents();
    }

    @Override
    public void onStop() {
        //stop getting events
        if(getEventsSubscription != null && !getEventsSubscription.isUnsubscribed()) {
            getEventsSubscription.unsubscribe();
        }
        if(deleteEventSubscription != null && !deleteEventSubscription.isUnsubscribed()) {
            deleteEventSubscription.unsubscribe();
        }
    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(IEventsView view) {
        this.eventsView = view;
        getEvents();
    }

    private void getEvents() {
        eventsView.setLoading(true);
        getEventsSubscription = getEventsUseCase.execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(new Func1<Throwable, List<Event>>() {
                    @Override
                    public List<Event> call(Throwable throwable) {
                        //there was an error while getting events
                        throwable.printStackTrace();
                        return null;
                    }
                })
                .subscribe(new Action1<List<Event>>() {
                    @Override
                    public void call(List<Event> events) {
                        if(events != null) {
                            Log.i("MAIN", Integer.toString(events.size()));
                            groupedEvents = groupEventsByDay(events);
                            eventsView.setLoading(false);
                            eventsView.showEvents(groupedEvents);
                        }
                    }
                });
    }

    private HashMap<String, List<Event>> groupEventsByDay(List<Event> events) {
        //we use a LinkedHashMap to preserve insertion order
        HashMap<String, List<Event>> result = new LinkedHashMap<>();
        SimpleDateFormat withoutTimeDateFormat = new SimpleDateFormat("yyyy.MM.dd");

        for(Event e : events) {
            String key = withoutTimeDateFormat.format(e.getStartDate());
            if(result.get(key) == null) { //event on a new, unused day
                result.put(key, new ArrayList<Event>());
            }
            result.get(key).add(e);
        }

        return  result;
    }

    @Override    public void floatingActionButtonOnClick() {
        eventsView.navigateToAddEventView();
    }

    @Override
    public void onEventDeleted(final Event event) {
        deleteEventUseCase.setId(event.getId());
        deleteEventSubscription = deleteEventUseCase.execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(new Func1<Throwable, Integer>() {
                    @Override
                    public Integer call(Throwable throwable) {
                        throwable.printStackTrace();
                        return null;
                    }
                })
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer integer) {
                        //maybe show if it was successful
                        Log.i("MAIN", "Deleted the event!");
//                        SimpleDateFormat withoutTimeDateFormat = new SimpleDateFormat("yyyy.MM.dd");
//                        String key = withoutTimeDateFormat.format(event.getStartDate());
//                        groupedEvents.get(key).remove(event);
//                        if(groupedEvents.get(key).size() == 0)
//                            groupedEvents.remove(key);
//                        eventsView.showEvents(groupedEvents);
                        getEvents();
                    }
                });
    }

    @Override
    public void onEventAdded(String serializedEvent) {
        //get new events when we come back from adding
        //getEvents();
//        SimpleDateFormat withoutTimeDateFormat = new SimpleDateFormat("yyyy.MM.dd");
//        Event event = new Gson().fromJson(serializedEvent, Event.class);
//        String key = withoutTimeDateFormat.format(event.getStartDate());
//        if(!groupedEvents.containsKey(key))
//            groupedEvents.put(key, new ArrayList<Event>());
//
//        groupedEvents.get(key).add(event);
//        Collections.sort(groupedEvents.get(key), new Comparator<Event>() {
//            @Override
//            public int compare(Event e1, Event e2) {
//                return e1.getStartDate().compareTo(e2.getStartDate());
//            }
//        });
//        eventsView.showEvents(groupedEvents);
        getEvents();
    }
}
