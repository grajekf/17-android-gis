package pl.edu.pw.mini.grastudio.reminder.domain;

import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.repository.IRepository;
import rx.Observable;

public class GetEventsUseCase implements IUseCase<List<Event>> {
    private IRepository<Event> eventRepository;

    public GetEventsUseCase(IRepository<Event> eventRepository) {
        this.eventRepository = eventRepository;
    }

    @Override
    public Observable<List<Event>> execute() {
        return eventRepository.getAll();
    }
}
