package pl.edu.pw.mini.grastudio.reminder.presenter;

import java.util.Date;

import pl.edu.pw.mini.grastudio.reminder.view.ILocationsView;

/**
 * Created by PC DELL on 31.05.2017.
 */

public interface ILocationsPresenter extends IPresenter<ILocationsView> {
    void onMapReady();
    void OnlyTodayEvents();
    void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults);
    void onConnected();
    void onLocationChanged();
    void insert(String s, double latitude, double longitude);
    void onLocationClick(long locationId);
    void updateMarkers();

    void filterEvents(Date startDate, Date endDate);
}
