package pl.edu.pw.mini.grastudio.reminder.utils;


import java.util.Calendar;
import java.util.Date;

public class DateUtils {
    public static int getDayOfTheMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    public static Date createDate(int year, int month, int day, int hour, int minute, int seconds) {
        return new Date(year - 1900, month - 1, day, hour, minute, seconds);
    }
}
