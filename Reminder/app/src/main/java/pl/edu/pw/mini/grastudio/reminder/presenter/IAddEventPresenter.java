package pl.edu.pw.mini.grastudio.reminder.presenter;


import com.google.android.gms.location.places.Place;

import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.view.IAddEventView;

public interface IAddEventPresenter extends IPresenter<IAddEventView> {
    void pickNewStartDate(String currentDate);
    void setNewStartDate(int year, int month, int dayOfMonth);
    void pickNewEndDate(String currentDate);
    void setNewEndDate(int year, int month, int dayOfMonth);

    void pickNewStartTime(String currentTime);
    void setNewStartTime(int hour, int minute);
    void pickNewEndTime(String currentTime);
    void setNewEndTime(int hour, int minute);
    void onAllDayCheckChanged(boolean isAllDay);

    void addOrUpdateEvent(String name, String startDate, String endDate, String startTime,
                          String endTime, boolean isAllDay, int durationHours, int durationMinutes,
                          int selectedPriorityIndex, String distance, int selectedTransportIndex);
    void cancelEditing();
    void pickPlace();
    void onPlaceSelected(long id, String name, String address, String latLng, double lat, double lng, boolean isFav);
    void setNewDuration(int durationHours, int durationMinutes);
    void newTitle(String title);
    void setEvent(Event event);
    void initPriority(String[] priorityNames);
}
