package pl.edu.pw.mini.grastudio.reminder.domain;


import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.repository.IEventsLocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.repository.IRepository;
import rx.Observable;
import rx.functions.Func1;

public class DeleteEventUseCase implements IDeleteEventUseCase {
    private IRepository<Event> eventRepository;
    private IEventsLocationsRepository eventsLocationsRepository;

    private long id;

    public DeleteEventUseCase(IRepository<Event> eventRepository, IEventsLocationsRepository eventsLocationsRepository) {
        this.eventRepository = eventRepository;
        this.eventsLocationsRepository = eventsLocationsRepository;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public Observable<Integer> execute() {
        //first delete all links, then the event
        return eventsLocationsRepository.delete(id).flatMap(new Func1<Integer, Observable<Integer>>() {
            @Override
            public Observable<Integer> call(Integer integer) {
                return eventRepository.delete(id);
            }
        });
    }
}
