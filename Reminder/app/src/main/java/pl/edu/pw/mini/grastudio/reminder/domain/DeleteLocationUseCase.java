package pl.edu.pw.mini.grastudio.reminder.domain;


import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.repository.IEventsLocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.repository.IRepository;
import rx.Observable;
import rx.functions.Func1;

public class DeleteLocationUseCase implements IDeleteLocationUseCase {

    private IEventsLocationsRepository eventsLocationsRepository;
    private IRepository<CustomerLocation> locationRepository;
    private CustomerLocation location;


    public DeleteLocationUseCase(IEventsLocationsRepository eventsLocationsRepository, IRepository<CustomerLocation> locationRepository) {
        this.eventsLocationsRepository = eventsLocationsRepository;
        this.locationRepository = locationRepository;
    }

    @Override
    public Observable<Integer> execute() {
        //first delete all links, then the location
        return eventsLocationsRepository.delete(location.getId()).flatMap(new Func1<Integer, Observable<Integer>>() {
            @Override
            public Observable<Integer> call(Integer integer) {
                return locationRepository.delete(location.getId());
            }
        });
    }

    @Override
    public void setLocation(CustomerLocation location) {
        this.location = location;
    }
}
