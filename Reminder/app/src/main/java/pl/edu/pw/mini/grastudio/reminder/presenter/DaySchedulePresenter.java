package pl.edu.pw.mini.grastudio.reminder.presenter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.domain.IGetLocalizedNotAllDayEvents;
import pl.edu.pw.mini.grastudio.reminder.googleApi.ILocationHelper;
import pl.edu.pw.mini.grastudio.reminder.model.LocalizedEvent;
import pl.edu.pw.mini.grastudio.reminder.view.IDayScheduleView;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class DaySchedulePresenter implements IDaySchedulePresenter {
    private IDayScheduleView dayScheduleView;
    private IGetLocalizedNotAllDayEvents getLocalizedNotAllDayEvents;
    private ILocationHelper locationHelper;
    private Date date;

    private Subscription getLocalizedNotAllDayEventsSubscription;

    public DaySchedulePresenter(IGetLocalizedNotAllDayEvents getLocalizedNotAllDayEvents, ILocationHelper locationHelper, Date date) {
        this.getLocalizedNotAllDayEvents = getLocalizedNotAllDayEvents;
        this.locationHelper = locationHelper;
        this.date = date;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        //stop getting events
        if(getLocalizedNotAllDayEventsSubscription != null && !getLocalizedNotAllDayEventsSubscription.isUnsubscribed()) {
            getLocalizedNotAllDayEventsSubscription.unsubscribe();
        }
    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(IDayScheduleView view) {
        dayScheduleView = view;
    }

    @Override
    public void onLocationServiceConnected() {
        getLocalizedNotAllDayEvents.setFirstLocation(locationHelper.getCurrentLatLng());
        getLocalizedNotAllDayEvents.setDate(date);
        getLocalizedNotAllDayEventsSubscription = getLocalizedNotAllDayEvents.execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(new Func1<Throwable, List<LocalizedEvent>>() {
                    @Override
                    public List<LocalizedEvent> call(Throwable throwable) {
                        throwable.printStackTrace();
                        return null;
                    }
                })
                .subscribe(new Action1<List<LocalizedEvent>>() {
                    @Override
                    public void call(List<LocalizedEvent> localizedEvents) {
                        if(localizedEvents != null) {
//                            dayScheduleView.setEvents(localizedEvents);
                            dayScheduleView.setGroupedEvents(getEventsDividedInRows(localizedEvents));
                        }
                    }
                });
    }

    private List<List<LocalizedEvent>> getEventsDividedInRows(List<LocalizedEvent> events) {
        List<List<LocalizedEvent>> result = new ArrayList<>();
        //add initial row
        result.add(new ArrayList<LocalizedEvent>());

        if(events != null && events.size() > 0) {
            result.get(0).add(events.get(0));
            for(int i = 1; i < events.size(); i++) {
                int currentRow = 0;
                while(currentRow < result.size() && events.get(i).intersectsWithAny(result.get(currentRow))) {
                    currentRow++;
                }
                if(currentRow == result.size())
                    result.add(new ArrayList<LocalizedEvent>());
                result.get(currentRow).add(events.get(i));

            }
        }

        return result;
    }
}
