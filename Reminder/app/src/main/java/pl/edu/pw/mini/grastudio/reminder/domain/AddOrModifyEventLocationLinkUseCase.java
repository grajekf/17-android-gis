package pl.edu.pw.mini.grastudio.reminder.domain;

import pl.edu.pw.mini.grastudio.reminder.model.EventLocationLink;
import pl.edu.pw.mini.grastudio.reminder.repository.IEventsLocationsRepository;
import rx.Observable;

/**
 * Created by Kamil on 22.04.2017.
 */

public class AddOrModifyEventLocationLinkUseCase implements IAddOrModifyEventLocationLinkUseCase {

    private IEventsLocationsRepository repository;
    private EventLocationLink linkToAdd;

    public AddOrModifyEventLocationLinkUseCase(IEventsLocationsRepository repository) {
        this.repository = repository;
    }

    @Override
    public void setLinkToAdd(EventLocationLink linkToAdd) {
        this.linkToAdd = linkToAdd;
    }

    @Override
    public Observable<Long> execute() {
        return repository.addOrUpdate(linkToAdd);
    }
}
