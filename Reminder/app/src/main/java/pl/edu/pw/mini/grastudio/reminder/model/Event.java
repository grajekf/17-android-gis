package pl.edu.pw.mini.grastudio.reminder.model;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Event extends Entity {

    private Date startDate;
    private Date endDate;
    private String name;
    private boolean isAllDay;
    private Priority priority;
    private final int[] keyToColor = {};
    protected int[] colors;
    private int durationHours;
    private int durationMinutes;
    private Transport meanOfTransport = Transport.bus;
    private double reminderDistanceMeters = 30;
    private final int MILLISECONDS_IN_HOUR = 1000 * 60 * 60;
    protected final int MILLISECONDS_IN_MINUTE = 1000 * 60;
    private final int MINUTES_IN_HOUR = 60;
    private final int HOURS_IN_DAY = 24;

    public Event(String name, Date startDate, Date endDate, boolean isAllDay, int priority) {
        super(-1);
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.isAllDay = isAllDay;
        this.meanOfTransport = meanOfTransport;
        this.reminderDistanceMeters = reminderDistanceMeters;
        setPriority(priority);
    }

    public Event(long id, String name, Date startDate, Date endDate, boolean isAllDay, int priority) {
        super(id);
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.isAllDay = isAllDay;
        this.meanOfTransport = meanOfTransport;
        this.reminderDistanceMeters = reminderDistanceMeters;
        setPriority(priority);
    }

    public Event(String name, Date startDate, Date endDate, boolean isAllDay, Priority priority) {
        super(-1);
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.isAllDay = isAllDay;
        this.meanOfTransport = meanOfTransport;
        this.reminderDistanceMeters = reminderDistanceMeters;
        setPriority(priority);
    }

    public Event(long Id, String name, Date startDate, Date endDate, boolean isAllDay, Priority priority) {
        super(Id);
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.isAllDay = isAllDay;
        setPriority(priority);
    }
    //returns duration in milliseconds
    public long getDuration() {
        return  endDate.getTime() - startDate.getTime();
    }

    public int getDurationInMinutes() {
        return (int)(getDuration() / MILLISECONDS_IN_MINUTE);
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAllDay() {
        return isAllDay;
    }

    public void setAllDay(boolean allDay) {
        isAllDay = allDay;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public int getPriorityInt() {
        return priority.ordinal();
    }

    public void setPriority(int value) {
        if(value < Priority.values().length)
            priority = Priority.values()[value];
        else
            priority = Priority.veryLow;
    }

    public int getColor() {
        if(colors == null)
            return -1;
        return colors[priority.ordinal()];
    }

    public void setPossibleColors(int[] colors) {
        this.colors = colors;
    }

    public int getDurationHours() {
        return durationHours;
    }

    public void setDurationHours(int durationHours) {
        this.durationHours = durationHours;
    }

    public int getDurationMinutes() {
        return durationMinutes;
    }

    public void setDurationMinutes(int durationMinutes) {
        this.durationMinutes = durationMinutes;
    }

    public String getRFC2445Duration() {
        return "+P" + String.valueOf(durationHours) + "H" + String.valueOf(durationMinutes) + "M";
    }

    public Transport getMeanOfTransport() {
        return meanOfTransport;
    }

    public void setMeanOfTransport(Transport meanOfTransport) {
        this.meanOfTransport = meanOfTransport;
    }

    public void setMeanOfTransport(int value) {
        if(value >= 0 && value < Priority.values().length)
            meanOfTransport = Transport.values()[value];
        else
            meanOfTransport = Transport.bus;
    }


    public int getTransportInt() {
        return meanOfTransport.ordinal();
    }

    public double getReminderDistanceMeters() {
        return reminderDistanceMeters;
    }

    public void setReminderDistanceMeters(double reminderDistanceMeters) {
        this.reminderDistanceMeters = reminderDistanceMeters;
    }

    public void setRFC2445Duration(String duration) {
        if(duration == null)
            return;
        Pattern p = Pattern.compile("-?\\d+");
        Matcher m = p.matcher(duration);
        //find hours
        boolean finished = !m.find();
        durationHours = Integer.parseInt(m.group());
        if(!finished) {
            finished = !m.find();
            durationMinutes = Integer.parseInt(m.group());
        }
    }

    public enum Priority {
        veryLow,
        low,
        normal,
        high,
        crucial;
    }

    public enum Transport {
        foot,
        bike,
        car,
        bus
    }
}
