package pl.edu.pw.mini.grastudio.reminder.database;

import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.model.EventLocationLink;


public interface IDatabaseHandler {
    //Adding new location
    long addLocation(CustomerLocation location);

    long addEventLocation(EventLocationLink link);

    long updateEventLocation(EventLocationLink link);

    //Getting single location
    CustomerLocation getLocation(Long id);

    EventLocationLink getEventLocationFromId(Long id);

    EventLocationLink getEventLocationFromEventId(Long eventId);

    List<EventLocationLink> getEventLocationFromLocationId(Long locationId);

    //Getting all locations
    List<CustomerLocation> getAllLocations();

    List<EventLocationLink> getAllEventLocationLinks();

    //Getting location count
    int getLocationCount();

    //Updating single location
    long updateLocation(CustomerLocation location);

    //Deleting single shopping item
    int deleteLocation(long id);

    int deleteEventLocationLink(long id);

    void addEvent(Event event);

    void updateEvent(Event event);

    void getEvent(Event event);
}
