package pl.edu.pw.mini.grastudio.reminder;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.edu.pw.mini.grastudio.reminder.database.DatabaseHandler;
import pl.edu.pw.mini.grastudio.reminder.domain.GetLocalizedNotAllDayEvents;
import pl.edu.pw.mini.grastudio.reminder.googleApi.ILocationHelper;
import pl.edu.pw.mini.grastudio.reminder.googleApi.LocationHelper;
import pl.edu.pw.mini.grastudio.reminder.googleApi.RouteHelper;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.model.LocalizedEvent;
import pl.edu.pw.mini.grastudio.reminder.presenter.DaySchedulePresenter;
import pl.edu.pw.mini.grastudio.reminder.presenter.IDaySchedulePresenter;
import pl.edu.pw.mini.grastudio.reminder.repository.EventRepository;
import pl.edu.pw.mini.grastudio.reminder.repository.EventsLocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.repository.LocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.view.IDayScheduleView;


public class DayScheduleFragment extends Fragment implements IDayScheduleView, GoogleApiClient.ConnectionCallbacks, View.OnClickListener {
    private static final String ARG_DATE = "date";
    public static final String TAG = "daySchedule";

    private Date date;
    private IDaySchedulePresenter presenter;

    @BindView(R.id.schedule_grid)
    GridLayout grid;

    private static final int ROW_MINUTES = 15;
    private static final int MINUTES_PER_DAY = 24 * 60;
    private static final int ROWS = MINUTES_PER_DAY / ROW_MINUTES;
    private static final int ROWS_PER_HOUR = ROWS / 24;
    private static final int ADDITIONAL_COLUMNS = 2;


    public DayScheduleFragment() {
        // Required empty public constructor
    }

    public static DayScheduleFragment newInstance(Date date) {
        DayScheduleFragment fragment = new DayScheduleFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_DATE, date.getTime());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            date = new Date(getArguments().getLong(ARG_DATE));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_day_schedule, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        ButterKnife.bind(this, getView());

//        initializeRecycleView();
        initializePresenter();
        initializeGrid();

        getView().setBackgroundColor(Color.WHITE);
        getView().setClickable(true);

        //remove tabs
        getActivity().findViewById(R.id.tab_layout).setVisibility(View.GONE);
        ((AppCompatActivity) getActivity()).findViewById(R.id.fab).setVisibility(View.GONE);
        getActivity().findViewById(R.id.toolbar).setVisibility(View.VISIBLE);
    }

    @Override
    public void onStart(){
        super.onStart();

        ((AppCompatActivity) getActivity()).findViewById(R.id.tab_layout).setVisibility(View.GONE);
        ((AppCompatActivity) getActivity()).findViewById(R.id.fab).setVisibility(View.GONE);
        getActivity().findViewById(R.id.toolbar).setVisibility(View.VISIBLE);

        SimpleDateFormat format = new SimpleDateFormat("dd MMMM");

        getActivity().setTitle(getActivity().getString(R.string.schedule_title, format.format(date)));

        presenter.onStart();
    }

    @Override
    public void onStop() {
        presenter.onStop();
        ((AppCompatActivity) getActivity()).findViewById(R.id.tab_layout).setVisibility(View.VISIBLE);
        ((AppCompatActivity) getActivity()).findViewById(R.id.fab).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.toolbar).setVisibility(View.GONE);
        super.onStop();
    }


    private void initializePresenter() {
        presenter = new DaySchedulePresenter(new GetLocalizedNotAllDayEvents(
                new EventRepository(getActivity(), new DatabaseHandler(getActivity())),
                new EventsLocationsRepository(new DatabaseHandler(getActivity())),
                new LocationsRepository(new DatabaseHandler(getActivity())),
                new RouteHelper(getActivity())),
                new LocationHelper(getActivity(), this),
                date);
        presenter.attachView(this);
    }

    private void initializeGrid() {
        grid.setRowCount(ROWS);
        for(int i = 0; i < 24; i++) {
            View hourView = getActivity().getLayoutInflater().inflate(R.layout.schedule_hour, null);
            TextView hourText = (TextView)hourView.findViewById(R.id.hour_text);
            hourText.setText(String.valueOf(i) + ":00");
            GridLayout.Spec rowSpec = GridLayout.spec(i * ROWS_PER_HOUR, ROWS_PER_HOUR, 1f);
            GridLayout.Spec columnSpec = GridLayout.spec(1, 1, 1f);
            GridLayout.LayoutParams gridParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
            grid.addView(hourView, gridParams);

//            View dividerView = getActivity().getLayoutInflater().inflate(R.layout.schedule_divider, null);
//            rowSpec = GridLayout.spec(i * ROWS_PER_HOUR, ROWS_PER_HOUR, 1f);
//            columnSpec = GridLayout.spec(2, 1, 5f);
//            gridParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
//            grid.addView(dividerView, gridParams);
        }
        for(int i = 0; i < ROWS; i++) {
            View spaceView = getActivity().getLayoutInflater().inflate(R.layout.schedule_space, null);
            GridLayout.Spec rowSpec = GridLayout.spec(i, 1, 1f);
            GridLayout.Spec columnSpec = GridLayout.spec(0, 1, 0f);
            GridLayout.LayoutParams gridParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
            grid.addView(spaceView, gridParams);
        }
    }

    @Override
    public void setEvents(List<LocalizedEvent> localizedEvents) {
        for(LocalizedEvent e : localizedEvents) {
            View eventView = getActivity().getLayoutInflater().inflate(R.layout.schedule_event, null);
            TextView titleText = (TextView)eventView.findViewById(R.id.event_name);
            titleText.setText(e.getName());
            titleText.setBackgroundColor(e.getColor());
            int minutesFromMidnight = e.getStartDate().getHours() * 60 + e.getStartDate().getMinutes();
            GridLayout.Spec rowSpec = GridLayout.spec((int)getGridRowFromTime(minutesFromMidnight),
                    (int)Math.ceil(getGridRowFromTime(e.getDurationInMinutes())), 1f);
            GridLayout.Spec columnSpec = GridLayout.spec(2, 1, 5f);
            GridLayout.LayoutParams gridParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
            grid.addView(eventView, gridParams);
            //add travel time
            eventView = getActivity().getLayoutInflater().inflate(R.layout.schedule_event, null);
            eventView.setBackgroundColor(Color.parseColor("#ffc107"));
            rowSpec = GridLayout.spec((int)getGridRowFromTime(minutesFromMidnight - e.getTravelTime()),
                    ((int)getGridRowFromTime(minutesFromMidnight) - (int)getGridRowFromTime(minutesFromMidnight - e.getTravelTime())), 1f);
            columnSpec = GridLayout.spec(2, 1, 5f);
            gridParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
            grid.addView(eventView, gridParams);
        }
    }

    @Override
    public void setGroupedEvents(List<List<LocalizedEvent>> eventsDividedInRows) {
        grid.setColumnCount(eventsDividedInRows.size() + ADDITIONAL_COLUMNS);
        for(int i = 0; i < eventsDividedInRows.size(); i++) {
            for(LocalizedEvent e : eventsDividedInRows.get(i)) {
                View eventView = getActivity().getLayoutInflater().inflate(R.layout.schedule_event, null);
                TextView titleText = (TextView)eventView.findViewById(R.id.event_name);
                titleText.setText(e.getName());
                titleText.setBackgroundColor(getResources().getColor(R.color.priorityNormal));
                titleText.setTag(e);
                titleText.setOnClickListener(this);
                int minutesFromMidnight = e.getStartDate().getHours() * 60 + e.getStartDate().getMinutes();
                GridLayout.Spec rowSpec = GridLayout.spec((int)getGridRowFromTime(minutesFromMidnight),
                        (int)Math.ceil(getGridRowFromTime(e.getDurationInMinutes())), 1f);
                GridLayout.Spec columnSpec = GridLayout.spec(i + ADDITIONAL_COLUMNS, 1, 5f);
                GridLayout.LayoutParams gridParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
                grid.addView(eventView, gridParams);
                //add travel time
                eventView = getActivity().getLayoutInflater().inflate(R.layout.schedule_event, null);
                titleText = (TextView)eventView.findViewById(R.id.event_name);
                titleText.setBackgroundColor(Color.parseColor("#ffc107"));
                rowSpec = GridLayout.spec((int)getGridRowFromTime(minutesFromMidnight - e.getTravelTime()),
                        ((int)getGridRowFromTime(minutesFromMidnight) - (int)getGridRowFromTime(minutesFromMidnight - e.getTravelTime())), 1f);
                columnSpec = GridLayout.spec(i + ADDITIONAL_COLUMNS, 1, 5f);
                gridParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
                grid.addView(eventView, gridParams);
            }
        }

    }

    private double getGridRowFromTime(int minutesFromMidnight) {
        return (double)minutesFromMidnight * ROWS / MINUTES_PER_DAY;
    }

    @Override
    public void onClick(View v) {
        Event event = (Event)v.getTag();
        String oldTitle = getActivity().getTitle().toString();
        EventDetailsFragment frag = EventDetailsFragment.newInstance(event, oldTitle, false);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.content, frag, EventDetailsFragment.TAG).addToBackStack(EventDetailsFragment.TAG);
        transaction.commit();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        presenter.onLocationServiceConnected();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
