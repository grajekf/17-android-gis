package pl.edu.pw.mini.grastudio.reminder.googleApi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.util.Log;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Leg;
import com.akexorcist.googledirection.model.Route;
import com.akexorcist.googledirection.model.RoutePolyline;
import com.akexorcist.googledirection.model.Step;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import pl.edu.pw.mini.grastudio.reminder.IMessage;
import pl.edu.pw.mini.grastudio.reminder.R;
import pl.edu.pw.mini.grastudio.reminder.RouteActivity;
import rx.Observable;
import rx.Subscriber;

import static com.google.android.gms.tasks.Tasks.await;

/**
 * Created by Kamil on 30.04.2017.
 */

public  class RouteHelper implements IRouteHelper {



     final int timeBefore = 300;
     Context context;

    public  RouteHelper(Context context) {

        this.context = context;
    }

    @Override
    public void TimeFromPointToPoint(LatLng origin, LatLng destination, final int time,  final IMessage message, int m){//CustomerLocation source, CustomerLocation destination){


        GoogleDirection.withServerKey("AIzaSyAO0gn8ecqleMAWmnqQUHTfJIRE2CmnDeE")
                .from(origin)
                .to(destination)
                .transportMode(travelingMode(m))
              .execute(new DirectionCallback() {
                    @Override
                    public void onDirectionSuccess(Direction direction, String rawBody) {
                        // Do something here
                        try {
                            Log.d("INFO", rawBody);
                            List<Route> routes = direction.getRouteList();
                            List<Leg> legs = routes.get(0).getLegList();
                            //legs.get(0).getDuration().getText().toString();

                       // JSONObject routes = new JSONObject(all.getString("routes"));
                        //JSONObject legs = routes.getJSONObject("bounds");

                        //Log.d("INFO", rawBody);
                         //   Log.d("INFO", legs.get(0).getDuration().getText().toString());
                            if(time <= Integer.parseInt(legs.get(0).getDuration().getValue()) + timeBefore)
                            message.ShowMessage();
                            //locationsView.toastMessage(legs.get(0).getDuration().getValue().toString());
                        }catch (Exception e){
                            Log.d("INFO", e.getMessage().toString());
                        }
                    }

                    @Override
                    public void onDirectionFailure(Throwable t) {
                        // Do something here
                    }
                });
    }

    @Override
    public Observable<Integer> ObservableTimeFromPointToPoint(final LatLng origin, final LatLng destination, final int transportType) {
        return  Observable.create(new Observable.OnSubscribe<Integer>() {
            @Override
            public void call(final Subscriber<? super Integer> subscriber) {
                GoogleDirection.withServerKey("AIzaSyAO0gn8ecqleMAWmnqQUHTfJIRE2CmnDeE")
                        .from(origin)
                        .to(destination)
                        .transportMode(travelingMode(transportType))
                        .execute(new DirectionCallback() {
                            @Override
                            public void onDirectionSuccess(Direction direction, String rawBody) {
                                // Do something here
                                try {
                                    Log.d("INFO", rawBody);
                                    List<Route> routes = direction.getRouteList();
                                    List<Leg> legs = routes.get(0).getLegList();
                                    subscriber.onNext(Integer.parseInt(legs.get(0).getDuration().getValue()));
                                    subscriber.onCompleted();
                                }catch (Exception e){
                                    Log.d("INFO", e.getMessage().toString());
                                    subscriber.onError(e);
                                }
                            }

                            @Override
                            public void onDirectionFailure(Throwable t) {
                                // Do something here
                                t.printStackTrace();
                                subscriber.onError(t);
                            }
                        });
            }
        });
    }

    public void TimeFromPointToPoint(LatLng origin, LatLng destination, final TabLayout.Tab tab, int m){//CustomerLocation source, CustomerLocation destination){


        GoogleDirection.withServerKey("AIzaSyAO0gn8ecqleMAWmnqQUHTfJIRE2CmnDeE")
                .from(origin)
                .to(destination)
                .transportMode(travelingMode(m))
                .execute(new DirectionCallback() {
                    @Override
                    public void onDirectionSuccess(Direction direction, String rawBody) {
                        // Do something here
                        try {
                            Log.d("INFO", rawBody);
                            List<Route> routes = direction.getRouteList();
                            List<Leg> legs = routes.get(0).getLegList();
                            //legs.get(0).getDuration().getText().toString();

                            // JSONObject routes = new JSONObject(all.getString("routes"));
                            //JSONObject legs = routes.getJSONObject("bounds");

                            //Log.d("INFO", rawBody);
                            //   Log.d("INFO", legs.get(0).getDuration().getText().toString());
                            tab.setText(legs.get(0).getDuration().getText());
                            //locationsView.toastMessage(legs.get(0).getDuration().getValue().toString());
                        }catch (Exception e){
                            Log.d("INFO", e.getMessage().toString());
                        }
                    }

                    @Override
                    public void onDirectionFailure(Throwable t) {
                        // Do something here
                    }
                });
    }

    @Override
    public void RouteFromPointToPoint(LatLng origin, LatLng destination) {

    }


    public void RouteFromPointToPoint(LatLng origin, LatLng destination, final RouteActivity routeActivity, int m) {
        GoogleDirection.withServerKey("AIzaSyAO0gn8ecqleMAWmnqQUHTfJIRE2CmnDeE")
                .from(origin)
                .to(destination)
                .transportMode(travelingMode(m))
                .execute(new DirectionCallback() {
                    @Override
                    public void onDirectionSuccess(Direction direction, String rawBody) {
                        // Do something here
                        try {
                        Log.d("INFO", rawBody);
                            //locationsView.toastMessage(legs.get(0).getDuration().getValue().toString());
                            List<Route> routes = direction.getRouteList();
                            List<Leg> legs = routes.get(0).getLegList();
                            //legs.get(0).getDuration().getText().toString();

                            // JSONObject routes = new JSONObject(all.getString("routes"));
                            //JSONObject legs = routes.getJSONObject("bounds");

                            //Log.d("INFO", rawBody);
                            //   Log.d("INFO", legs.get(0).getDuration().getText().toString());
                            List<PolylineOptions> polylineOptionsList = new ArrayList<PolylineOptions>();
                           for(Step step: legs.get(0).getStepList() ) {
                               PolylineOptions polylineOptions = new PolylineOptions();
                               for (LatLng latLng : step.getPolyline().getPointList()) {
                                   polylineOptions.add(latLng);
                               }
                               polylineOptionsList.add(polylineOptions);
                           }
                            routeActivity.drawRoutes(polylineOptionsList);
                        }catch (Exception e){
                            Log.d("INFO", e.getMessage().toString());
                        }
                    }

                    @Override
                    public void onDirectionFailure(Throwable t) {
                        // Do something here
                    }
                });
    }


    @Override
    public int DistanceFromPointToPoint(LatLng currentPosition, LatLng destination) {
        //compute distance
                try {
                    return  (int) SphericalUtil.computeDistanceBetween(currentPosition, destination);

                }catch (Exception e){
                    Log.d("INFO", e.getMessage());
                }
                return Integer.MAX_VALUE;
    }

    private String travelingMode(int m){
        int mode = m;

        if(mode == -1) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            mode = Integer.parseInt(sharedPreferences.getString(context.getString(R.string.traveling_mode_preferences), "3"));
        }

        switch (mode) {
            case 0:
                return TransportMode.WALKING;
            case 1:
                return TransportMode.BICYCLING;
            case 2:
                return TransportMode.DRIVING;
            case 3:
                return TransportMode.TRANSIT;
            default:
                return TransportMode.WALKING;
        }
    }
}
