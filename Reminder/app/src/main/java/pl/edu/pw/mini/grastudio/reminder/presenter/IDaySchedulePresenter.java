package pl.edu.pw.mini.grastudio.reminder.presenter;

import pl.edu.pw.mini.grastudio.reminder.view.IDayScheduleView;

/**
 * Created by Kamil on 27.04.2017.
 */

public interface  IDaySchedulePresenter extends IPresenter<IDayScheduleView>  {
    void onLocationServiceConnected();
}
