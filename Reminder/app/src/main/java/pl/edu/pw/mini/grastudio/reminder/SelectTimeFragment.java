package pl.edu.pw.mini.grastudio.reminder;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SelectTimeFragment extends Fragment {

    private static final String START_TIME = "startTime";
    private static final String END_TIME = "endTime";

    @BindView(R.id.endTimeTextView)
    public TextView endTimeTextView;
    @BindView(R.id.startTimeTextView)
    public TextView startTimeTextView;

    private String startTimeString;
    private String endTimeString;

    private OnClickTimeListener listener;

    public SelectTimeFragment() {
        // Required empty public constructor
    }

    public static SelectTimeFragment newInstance(String startTime, String endTime) {
        SelectTimeFragment fragment = new SelectTimeFragment();
        Bundle bundle = new Bundle();
        bundle.putString(START_TIME, startTime);
        bundle.putString(END_TIME, endTime);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            startTimeString = getArguments().getString(START_TIME);
            endTimeString = getArguments().getString(END_TIME);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ButterKnife.bind(this, getView());

        startTimeTextView.setText(startTimeString);
        endTimeTextView.setText(endTimeString);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_select_time, container, false);
        return view;
    }

    public void onStartTimeClick(View view) {
        if(listener != null)
            listener.onStartTimeClick(startTimeTextView.getText().toString());
    }

    public void onEndTimeClick(View view) {
        if(listener != null)
            listener.onEndTimeClick(endTimeTextView.getText().toString());
    }

    public String getStartTime() {
        return startTimeTextView.getText().toString();
    }

    public void setStartTime(String time) {
        startTimeString = time;
        if(startTimeTextView != null)
            startTimeTextView.setText(time);
    }

    public String getEndTime() {
        return endTimeTextView.getText().toString();
    }

    public void setEndTime(String time) {
        endTimeString = time;
        if(endTimeTextView!= null)
            endTimeTextView.setText(time);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnClickTimeListener) {
            listener = (OnClickTimeListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface OnClickTimeListener {
        void onStartTimeClick(String startDate);
        void onEndTimeClick(String endDate);
    }
}
