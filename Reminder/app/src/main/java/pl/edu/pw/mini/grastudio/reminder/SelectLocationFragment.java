package pl.edu.pw.mini.grastudio.reminder;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.edu.pw.mini.grastudio.reminder.adapter.SelectLocationAdapter;
import pl.edu.pw.mini.grastudio.reminder.database.DatabaseHandler;
import pl.edu.pw.mini.grastudio.reminder.domain.GetFavoriteLocationsUseCase;
import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.presenter.ISelectLocationPresenter;
import pl.edu.pw.mini.grastudio.reminder.presenter.SelectLocationPresenter;
import pl.edu.pw.mini.grastudio.reminder.repository.LocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.view.ISelectLocationView;

import static android.app.Activity.RESULT_OK;


public class SelectLocationFragment extends DialogFragment implements ISelectLocationView, SelectLocationAdapter.OnLocationSelectedListener {

    @BindView(R.id.locationTextView)
    TextView newLocationTextView;
    @BindView(R.id.favoriteList)
    RecyclerView favoriteRecyclerView;

    SelectLocationAdapter adapter;
    ISelectLocationPresenter selectLocationPresenter;
    OnLocationSelectedListener locationSelectedListener;

    class RequestCode {
        static final int PICK_LOCATION = 1;
    }


    static SelectLocationFragment newInstance() {
        SelectLocationFragment f = new SelectLocationFragment();

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ButterKnife.bind(this, getView());
        initializeRecyclerView();
        initializePresenter();
        newLocationTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    Intent intent = builder.build(getActivity());
                    startActivityForResult(intent, RequestCode.PICK_LOCATION);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.select_location_dialog, container, false);

        return v;
    }

    @Override
    public void setFavoriteList(List<CustomerLocation> customerLocations) {
        adapter.replaceData(customerLocations);
    }

    private void initializeRecyclerView(){
        favoriteRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        favoriteRecyclerView.setLayoutManager(mLayoutManager);
//        favoriteRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        favoriteRecyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new SelectLocationAdapter();
        adapter.attachListener(this);
        favoriteRecyclerView.setAdapter(adapter);
    }
    private void initializePresenter(){
        selectLocationPresenter = new SelectLocationPresenter(new GetFavoriteLocationsUseCase(new LocationsRepository(new DatabaseHandler(getActivity()))));
        selectLocationPresenter.attachView(this);
    }

    @Override
    public void onLocationSelected(CustomerLocation location) {
        finishWithResult(location.getId(), location.getName(),
                "", "", location.getLat(), location.getLng(), location.isFavorite());
    }

    private void finishWithResult(long id, String name, String address, String latLng, double lat, double lng, boolean isFav) {
        if(locationSelectedListener != null)
            locationSelectedListener.onLocationSelected(id, name, address, latLng, lat, lng, isFav);

        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestCode.PICK_LOCATION) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, getActivity());
                finishWithResult(-1, place.getName().toString(), place.getAddress().toString(),
                        place.getLatLng().toString(), place.getLatLng().latitude,
                        place.getLatLng().longitude, false);
            }
        }
    }

    public void attachListener(OnLocationSelectedListener locationSelectedListener) {
        this.locationSelectedListener = locationSelectedListener;
    }

    public interface OnLocationSelectedListener {
        void onLocationSelected(long id, String name, String address, String latLng, double lat, double lng, boolean isFav);
    }
}
