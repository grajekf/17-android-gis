package pl.edu.pw.mini.grastudio.reminder.domain;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.repository.IRepository;
import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;

/**
 * Created by Kamil on 07.06.2017.
 */

public class GetNotAllDayEventsForDateUseCase implements  IGetNotAllDayEventsForDateUseCase {

    private IRepository<Event> eventRepository;
    private Date date;

    public GetNotAllDayEventsForDateUseCase(IRepository<Event> eventRepository) {
        this.eventRepository = eventRepository;
    }

    @Override
    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public Observable<List<Event>> execute() {
        //we need to use flatMapIterable, because otherwise we cannot filter the list
        return eventRepository.getAll().flatMapIterable(new Func1<List<Event>, List<Event>>() {
            @Override
            public List<Event> call(List<Event> events) {
                return events;
            }
        }).filter(new Func1<Event, Boolean>() {
            //show only events after current date or events that last all day and are today
            @Override
            public Boolean call(Event event) {
                return  sameDate(date,event.getStartDate()) && !event.isAllDay();
            }
        }).toSortedList(new Func2<Event, Event, Integer>() { //sort chronologically
            @Override
            public Integer call(Event event, Event event2) {
                return  event.getStartDate().compareTo(event2.getStartDate());
            }
        });
    }

    private boolean sameDate(Date date1, Date date2){
        Calendar first = Calendar.getInstance();
        first.setTime(date1);

        Calendar second = Calendar.getInstance();
        second.setTime(date2);

        return first.get(Calendar.DAY_OF_YEAR) == second.get(Calendar.DAY_OF_YEAR) ;
    }


}
