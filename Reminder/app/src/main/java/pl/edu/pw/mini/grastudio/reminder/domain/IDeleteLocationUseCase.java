package pl.edu.pw.mini.grastudio.reminder.domain;

import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;

public interface IDeleteLocationUseCase extends IUseCase<Integer> {
    void setLocation(CustomerLocation location);
}
