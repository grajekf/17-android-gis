package pl.edu.pw.mini.grastudio.reminder.domain;

import com.google.android.gms.maps.model.LatLng;

import java.util.Date;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.LocalizedEvent;

public interface IGetLocalizedNotAllDayEvents extends IUseCase<List<LocalizedEvent>> {
    void setDate(Date date);
    void setFirstLocation(LatLng firstLocation);
}
