package pl.edu.pw.mini.grastudio.reminder.repository;

import java.util.List;
import java.util.concurrent.Callable;

import pl.edu.pw.mini.grastudio.reminder.database.IDatabaseHandler;
import pl.edu.pw.mini.grastudio.reminder.model.EventLocationLink;
import rx.Observable;

/**
 * Created by Kamil on 22.04.2017.
 */

public class EventsLocationsRepository implements IEventsLocationsRepository {

    private IDatabaseHandler db;

    public EventsLocationsRepository(IDatabaseHandler db) {
        this.db = db;
    }

    @Override
    public Observable<EventLocationLink> getById(final long id) {
        return Observable.fromCallable(new Callable<EventLocationLink>() {
            @Override
            public EventLocationLink call() throws Exception {
                return db.getEventLocationFromId(id);
            }
        });
    }

    @Override
    public Observable<List<EventLocationLink>> getAll() {
        return Observable.fromCallable(new Callable<List<EventLocationLink>>() {
            @Override
            public List<EventLocationLink> call() throws Exception {
                return db.getAllEventLocationLinks();
            }
        });
    }

    @Override
    public Observable<Long> addOrUpdate(final EventLocationLink item) {
        return Observable.fromCallable(new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                if(item.getId() == -1)
                    return db.addEventLocation(item);
                else
                    return db.updateEventLocation(item);
            }
        });
    }

    @Override
    public Observable<EventLocationLink> getByEventId(final long id) {
        return Observable.fromCallable(new Callable<EventLocationLink>() {
            @Override
            public EventLocationLink call() throws Exception {
                return db.getEventLocationFromEventId(id);
            }
        });
    }

    @Override
    public Observable<List<EventLocationLink>> getByLocationId(final long id) {
        return Observable.fromCallable(new Callable<List<EventLocationLink>>() {
            @Override
            public List<EventLocationLink> call() throws Exception {
                return db.getEventLocationFromLocationId(id);
            }
        });
    }

    //returns number of rows affected
    @Override
    public Observable<Integer> delete(final long id) {
        return Observable.fromCallable(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return db.deleteEventLocationLink(id);
            }
        });
    }
}
