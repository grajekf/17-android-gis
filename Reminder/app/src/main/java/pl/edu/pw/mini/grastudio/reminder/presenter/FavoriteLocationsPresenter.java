package pl.edu.pw.mini.grastudio.reminder.presenter;

import java.util.ArrayList;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.domain.IAddOrModifyLocationUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.IUseCase;
import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.view.IFavoriteView;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Kamil on 03.05.2017.
 */

public class FavoriteLocationsPresenter implements IFavoriteLocationsPresenter {

    private List<CustomerLocation> customerLocations = new ArrayList<>();
    private IFavoriteView favoriteView;
    private IUseCase<Long> addOrModifyLocationUseCase;
    private IUseCase<List<CustomerLocation>> getFavoriteLocationsUseCase;
    private IAddOrModifyLocationUseCase modifyLocationUseCase;

    public FavoriteLocationsPresenter(IUseCase<Long> addOrModifyLocationUseCase,
                                      IUseCase<List<CustomerLocation>> getFavoriteLocationsUseCase,
                                      IAddOrModifyLocationUseCase modifyLocationUseCase) {
        this.addOrModifyLocationUseCase = addOrModifyLocationUseCase;
        this.getFavoriteLocationsUseCase = getFavoriteLocationsUseCase;
        this.modifyLocationUseCase = modifyLocationUseCase;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(IFavoriteView view) {
        this.favoriteView = view;
        getLocations();
    }

    private void getLocations() {
        getFavoriteLocationsUseCase.execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(new Func1<Throwable, List<CustomerLocation>>() {
                    @Override
                    public List<CustomerLocation> call(Throwable throwable) {
                        throwable.printStackTrace();
                        return null;
                    }
                })
                .subscribe(new Action1<List<CustomerLocation>>() {
                    @Override
                    public void call(List<CustomerLocation> locations) {
                        favoriteView.showLocations(locations);
                    }
                });


    }

    @Override
    public void chooseNewFavoriteLocation() {
        favoriteView.startPlacePickerDialog();
    }

    @Override
    public void insertNewFavoriteLocation(String name, double lat, double lng) {
        final CustomerLocation location = new CustomerLocation(-1, name, lat, lng, true);
        ((IAddOrModifyLocationUseCase)addOrModifyLocationUseCase).setLocationToAdd(location);
        addOrModifyLocationUseCase.execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(new Func1<Throwable, Long>() {
                    @Override
                    public Long call(Throwable throwable) {
                        //there was an error while getting events
                        throwable.printStackTrace();
                        return null;
                    }
                })
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long id) {
                        location.setId(id);
                        favoriteView.addToRecyclerView(location);
                    }
                });
    }

    @Override
    public void onLocationAdded(String serializedData) {
        getLocations();
    }

    @Override
    public void onLocationDeleted(CustomerLocation location) {
        location.setFavorite(false);
        modifyLocationUseCase.setLocationToAdd(location);
        modifyLocationUseCase.execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(new Func1<Throwable, Long>() {
                    @Override
                    public Long call(Throwable throwable) {
                        throwable.printStackTrace();
                        return null;
                    }
                })
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long id) {
                        //updating was successful
                    }
                });
    }
}
