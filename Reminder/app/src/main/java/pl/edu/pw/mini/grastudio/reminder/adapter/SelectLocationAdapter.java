package pl.edu.pw.mini.grastudio.reminder.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.R;
import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;

public class SelectLocationAdapter extends RecyclerView.Adapter<SelectLocationAdapter.ViewHolder> {

    private List<CustomerLocation> locationsList;
    private OnLocationSelectedListener locationSelectedListener;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name;
        public ImageView image;
        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            image = (ImageView) itemView.findViewById(R.id.image_view);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if(position != RecyclerView.NO_POSITION) {
                if(locationSelectedListener != null) {
                    locationSelectedListener.onLocationSelected(locationsList.get(position));
                }
            }
        }
    }

    public SelectLocationAdapter() {
        this.locationsList = new ArrayList<>();
    }

    public void replaceData(List<CustomerLocation> locationsList){
        this.locationsList = locationsList;

        notifyDataSetChanged();
    }

    public void attachListener(OnLocationSelectedListener locationSelectedListener) {
        this.locationSelectedListener = locationSelectedListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.location_list_row, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CustomerLocation shoppingItem = locationsList.get(position);
        holder.name.setText(shoppingItem.getName());

        if(position == 0 ||
                locationsList.get(position - 1).getName().toUpperCase().charAt(0) !=
                        shoppingItem.getName().toUpperCase().charAt(0)) {
            ColorGenerator generator = ColorGenerator.MATERIAL;
            int color = generator.getColor(shoppingItem);
            String letter = String.valueOf(shoppingItem.getName().toUpperCase().charAt(0));
            TextDrawable drawable = TextDrawable.builder().buildRound(letter, color);
            holder.image.setImageDrawable(drawable);
        }
    }

    @Override
    public int getItemCount() {
        return locationsList.size();
    }

    public interface OnLocationSelectedListener{
        void onLocationSelected(CustomerLocation location);
    }

}
