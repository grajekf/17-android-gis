package pl.edu.pw.mini.grastudio.reminder.repository;


import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.Callable;

import pl.edu.pw.mini.grastudio.reminder.R;
import pl.edu.pw.mini.grastudio.reminder.database.IDatabaseHandler;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import rx.Observable;
import rx.functions.Func1;

//Asynchronous repository used to query and manipulate user events
public class EventRepository implements IRepository<Event> {

    public EventRepository(Context context, IDatabaseHandler db) {
        this.context = context;
        this.db = db;
    }

    //is needed for calendar queries, it should be enough to pass the current activity to it
    private Context context;
    //is needed for additional info for events
    private IDatabaseHandler db;

    //fields used for queries
    private final String[] EVENT_FIELDS = new String[]{
            CalendarContract.Events._ID,            //0
            CalendarContract.Events.TITLE,          //1
            CalendarContract.Events.DTSTART,        //2
            CalendarContract.Events.DTEND,          //3
            CalendarContract.Events.ALL_DAY,        //4
            CalendarContract.Events.CALENDAR_ID,    //5
            CalendarContract.Events.DELETED,        //6
            CalendarContract.Events.DISPLAY_COLOR,  //7
            CalendarContract.Events.EVENT_COLOR_KEY,//8
            CalendarContract.Events.DURATION        //9
    };

    private final int EVENT_ID_INDEX            =   0;
    private final int EVENT_TITLE_INDEX         =   1;
    private final int EVENT_DTSTART_INDEX       =   2;
    private final int EVENT_DTEND_INDEX         =   3;
    private final int EVENT_ALL_DAY_INDEX       =   4;
    private final int EVENT_CAL_ID_INDEX        =   5;
    private final int EVENT_DELETED_INDEX       =   6;
    private final int EVENT_COLOR_INDEX         =   7;
    private final int EVENT_COLOR_KEY_INDEX     =   8;
    private final int EVENT_DURATION_INDEX      =   9;
    //Used to only get user events, not holidays etc.
    //TODO: find out the users default calendar for real
    private final int DEFAULT_CALENDAR_ID = 1;

    //according to android documentation an event is all day if ALL_DAY is 1
    private final int ALL_DAY_INT = 1;
    //according to android documentation an event is deleted if DELETED is 1
    private final int DELETED_INT = 1;
    //if no priority is set we choose this
    private final int DEFAULT_PRIORITY = 2;


    @Override
    public Observable<Event> getById(final long id) {
        return getAll().flatMapIterable(new Func1<List<Event>, Iterable<? extends Event>>() {
            @Override
            public Iterable<? extends Event> call(List<Event> events) {
                return events;
            }
        }).filter(new Func1<Event, Boolean>() {
            @Override
            public Boolean call(Event event) {
                return event.getId() == id;
            }
        });
    }

    @Override
    public Observable<List<Event>> getAll() {
        //create a new observable with all the events
        return Observable.fromCallable(new Callable<List<Event>>() {
            @Override
            public List<Event> call() throws Exception {
                return getAllEvents();
            }
        });
    }

    private List<Event> getAllEvents() throws SecurityException {
        List<Event> result = new ArrayList<Event>();
        int[] possibleColors = new int[] {
                ContextCompat.getColor(context, R.color.priorityVeryLow),
                ContextCompat.getColor(context, R.color.priorityLow),
                ContextCompat.getColor(context, R.color.priorityNormal),
                ContextCompat.getColor(context, R.color.priorityHigh),
                ContextCompat.getColor(context, R.color.priorityCrucial),
        };
        Cursor cur = null;
        ContentResolver cr = context.getContentResolver();
        Uri uri = CalendarContract.Events.CONTENT_URI;
        String filterString = "((" + CalendarContract.Events.CALENDAR_ID
                + " = ?))";
        String[] filterArgs = new String[]
                {Integer.toString(DEFAULT_CALENDAR_ID)};
        try {
            //get the specified event fields, filter by calendar id and do not sort
            cur = cr.query(uri, EVENT_FIELDS, filterString, filterArgs, null);
            cur.moveToFirst();
        }catch (Exception e){
            Log.d("INFO", e.getMessage());
            return null;
        }
        //use the cursor to step through events
        while (cur.moveToNext()) {
            boolean isDeleted = cur.getInt(EVENT_DELETED_INDEX) == DELETED_INT;
            if(isDeleted) //skip deleted
                continue;
            long id = Long.parseLong(cur.getString(EVENT_ID_INDEX));
            String name = cur.getString(EVENT_TITLE_INDEX);
            boolean isAllDay = cur.getInt(EVENT_ALL_DAY_INDEX) == ALL_DAY_INT;
            //dates are returns as milliseconds since the unix epoch
            Date startDate = new Date(cur.getLong(EVENT_DTSTART_INDEX));
            Date endDate = new Date(cur.getLong(EVENT_DTEND_INDEX));
            int priority = DEFAULT_PRIORITY;
//            String priorityString = cur.getString(EVENT_COLOR_KEY_INDEX);
//            if(priorityString != null) {
//                priority = Integer.parseInt(priorityString);
//            }
//            String duration = cur.getString(EVENT_DURATION_INDEX);

            Event event = new Event(id, name, startDate, endDate, isAllDay, priority);
            db.getEvent(event);
            event.setPossibleColors(possibleColors);
            result.add(event);
        }
        Log.i("REPO", Integer.toString(result.size()));
        cur.close();

        return result;
    }

    @Override
    public Observable<Long> addOrUpdate(final Event item) {
        return Observable.fromCallable(new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                if(item.getId() == -1) //a new item
                    return insertEvent(item);
                else //an existing item
                    return updateEvent(item);
            }
        });

    }

    @Override
    public Observable<Integer> delete(final long id) {
        return Observable.fromCallable(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                ContentResolver cr = context.getContentResolver();
                ContentValues values = new ContentValues();
                Uri deleteUri = null;
                deleteUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, id);
                return cr.delete(deleteUri, null, null);
            }
        });

    }


    private Long updateEvent(Event event) throws SecurityException {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        Uri updateUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, event.getId());

        values.put(CalendarContract.Events.DTSTART, event.getStartDate().getTime());
        values.put(CalendarContract.Events.TITLE, event.getName());
        values.put(CalendarContract.Events.ALL_DAY, event.isAllDay());
        values.put(CalendarContract.Events.DTEND, event.getEndDate().getTime());

        cr.update(updateUri, values, null, null);
        db.updateEvent(event);

        return Long.parseLong(updateUri.getLastPathSegment());
    }

    private Long insertEvent(Event event) throws SecurityException {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();

        values.put(CalendarContract.Events.CALENDAR_ID, DEFAULT_CALENDAR_ID);
        values.put(CalendarContract.Events.DTSTART, event.getStartDate().getTime());
        values.put(CalendarContract.Events.TITLE, event.getName());
        values.put(CalendarContract.Events.ALL_DAY, event.isAllDay());
        values.put(CalendarContract.Events.DTEND, event.getEndDate().getTime());
        values.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().toString());

        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);
        event.setId(Long.parseLong(uri.getLastPathSegment()));
        db.addEvent(event);

        return event.getId();
    }
}
