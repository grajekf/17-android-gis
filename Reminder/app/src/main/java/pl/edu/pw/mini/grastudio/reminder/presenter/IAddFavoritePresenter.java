package pl.edu.pw.mini.grastudio.reminder.presenter;


import pl.edu.pw.mini.grastudio.reminder.view.IAddFavouriteView;

public interface IAddFavoritePresenter extends IPresenter<IAddFavouriteView> {
    void pickPlace();
    void onPlaceSelected(String name, String address, String latLng, double latitude, double longitude);

    void cancelEditing();

    void addOrUpdateFavorite(String name);
    boolean hasLocation();
}
