package pl.edu.pw.mini.grastudio.reminder.view;

import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;

public interface ISelectLocationView extends View {
    void setFavoriteList(List<CustomerLocation> customerLocations);
}
