package pl.edu.pw.mini.grastudio.reminder.view;

import android.view.*;

import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;

/**
 * Created by Kamil on 03.05.2017.
 */

public interface IFavoriteView extends View {
    void showLocations( List<CustomerLocation> customerLocations);
    void startPlacePickerDialog();
    void addToRecyclerView(CustomerLocation customerLocation);
}
