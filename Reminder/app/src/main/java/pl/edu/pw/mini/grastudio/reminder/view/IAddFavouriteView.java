package pl.edu.pw.mini.grastudio.reminder.view;


import android.location.Location;

import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;

public interface IAddFavouriteView extends View {
    void startPlacePickerDialog();
    void setLocationText(String text);
    void returnToLastScreen();
    void returnToLastScreenWithResult(String serializedResult);
}
