package pl.edu.pw.mini.grastudio.reminder.domain;

import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;

public interface IAddOrModifyLocationUseCase extends IUseCase<Long> {
    void setLocationToAdd(CustomerLocation locationToAdd);
}
