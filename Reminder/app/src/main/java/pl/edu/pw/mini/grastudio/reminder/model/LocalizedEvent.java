package pl.edu.pw.mini.grastudio.reminder.model;


import com.google.android.gms.maps.model.LatLng;

import java.util.Date;
import java.util.List;

public class LocalizedEvent extends Event {
    private CustomerLocation location;
    private int travelTime;

    public LocalizedEvent(Event event) {
        super(event.getId(),
                event.getName(),
                event.getStartDate(),
                event.getEndDate(),
                event.isAllDay(),
                event.getPriority());
        colors = event.colors;
    }
    public LocalizedEvent(LatLng latLng) {
        super(-1, null, null, null, false, 0);
        this.location = new CustomerLocation(-1, "Current Location", latLng.latitude, latLng.longitude);
    }

    public LocalizedEvent(String name, Date startDate, Date endDate, boolean isAllDay, int priority) {
        super(name, startDate, endDate, isAllDay, priority);
    }

    public LocalizedEvent(long id, String name, Date startDate, Date endDate, boolean isAllDay, int priority) {
        super(id, name, startDate, endDate, isAllDay, priority);
    }

    public LocalizedEvent(String name, Date startDate, Date endDate, boolean isAllDay, Priority priority) {
        super(name, startDate, endDate, isAllDay, priority);
    }

    public LocalizedEvent(long Id, String name, Date startDate, Date endDate, boolean isAllDay, Priority priority) {
        super(Id, name, startDate, endDate, isAllDay, priority);
    }

    public CustomerLocation getLocation() {
        return location;
    }

    public void setLocation(CustomerLocation location) {
        this.location = location;
    }

    public int getTravelTime() {
        return travelTime;
    }

    public Date getTravelStartTime() {
        return new Date(getStartDate().getTime() - travelTime * MILLISECONDS_IN_MINUTE);
    }

    public void setTravelTime(int travelTime) {
        this.travelTime = travelTime;
    }

    public boolean intersectsWith(LocalizedEvent other) {
        return getTravelStartTime().before(other.getTravelStartTime()) && getEndDate().after(other.getTravelStartTime())
                || getTravelStartTime().before(other.getEndDate()) && getEndDate().after(other.getTravelStartTime());
    }

    public boolean intersectsWithAny(List<LocalizedEvent> eventList) {
        boolean result = false;

        for(LocalizedEvent event : eventList) {
            if(intersectsWith(event))
                return true;
        }

        return result;
    }
}
