package pl.edu.pw.mini.grastudio.reminder.adapter;

import android.content.Context;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.edu.pw.mini.grastudio.reminder.R;
import pl.edu.pw.mini.grastudio.reminder.model.Event;

public class DateAdapter extends RecyclerView.Adapter<DateAdapter.EventCardViewHolder> {

    private Context context;
    private List<List<Event>> groupedEvents;
    private EventsAdapter.OnEventDeletedListener onEventDeletedListener;
    private EventsAdapter.OnClickedListener onClickedListener;
    private OnClickedListener onDateClickedListener;

    public DateAdapter(Context context, EventsAdapter.OnClickedListener onClickedListener) {
        this.context = context;
        this.onClickedListener = onClickedListener;
        this.groupedEvents = new ArrayList<>();
    }

    public void attachDeleteListener(EventsAdapter.OnEventDeletedListener onEventDeletedListener) {
        this.onEventDeletedListener = onEventDeletedListener;
    }

    public void attachListener(OnClickedListener onDateClickedListener) {
        this.onDateClickedListener = onDateClickedListener;
    }

    public DateAdapter(Context context, HashMap<String, List<Event>> groupedEventsMap) {
        this.context = context;
        this.groupedEvents = new ArrayList<>();
        //flatten map into list of list
        for(String key : groupedEventsMap.keySet()) {
            groupedEvents.add(groupedEventsMap.get(key));
        }
    }

    public void replaceData(HashMap<String, List<Event>> groupedEventsMap) {
        groupedEvents.clear();
        for(String key : groupedEventsMap.keySet()) {
            groupedEvents.add(groupedEventsMap.get(key));
        }
        Collections.sort(groupedEvents, new Comparator<List<Event>>() {
            @Override
            public int compare(List<Event> l1, List<Event> l2) {
                return l1.get(0).getStartDate().compareTo(l2.get(0).getStartDate());
            }
        });
        notifyDataSetChanged();
    }

    @Override
    public EventCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        //get view from layout
        View itemView = inflater.inflate(R.layout.date_card, parent, false);
        return new EventCardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(EventCardViewHolder holder, int position) {
        EventCardViewHolder viewHolder = (EventCardViewHolder) holder;
        List<Event> eventsForPosition = groupedEvents.get(position);
        SimpleDateFormat dayOfTheWeekDateFormat = new SimpleDateFormat("EEEE", context.getResources().getConfiguration().locale);
        SimpleDateFormat dateFormat = new SimpleDateFormat("d MMMM");

        if(eventsForPosition.size() > 0) {
            Date eventsDate = eventsForPosition.get(0).getStartDate();
            viewHolder.dayOfWeekTextView.setText(dayOfTheWeekDateFormat.format(eventsDate));
            viewHolder.dateTextView.setText(dateFormat.format(eventsDate));

            //create adapter for recycleView in child
            viewHolder.eventsRecycleView.setHasFixedSize(true);
            viewHolder.eventsRecycleView.setNestedScrollingEnabled(false);
            viewHolder.setIsRecyclable(false);
            viewHolder.eventsRecycleView.setLayoutManager(new LinearLayoutManager(context));
            EventsAdapter adapter = new EventsAdapter(context, eventsForPosition, onClickedListener);
            if(onEventDeletedListener != null)
                adapter.attachListener(onEventDeletedListener);
            viewHolder.eventsRecycleView.setAdapter(adapter);
            //create lines between items in child recycleView
//            viewHolder.eventsRecycleView.addItemDecoration(
//                    new DividerItemDecoration(viewHolder.eventsRecycleView.getContext(), DividerItemDecoration.VERTICAL));

            ItemTouchHelper.Callback callback = new ItemTouchHelperCallback(adapter);
            ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
            touchHelper.attachToRecyclerView(viewHolder.eventsRecycleView);

        }
    }

    @Override
    public int getItemCount() {
        return groupedEvents.size();
    }


    public class EventCardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.dayOfTheWeekTextView)
        public TextView dayOfWeekTextView;
        @BindView(R.id.dateTextView)
        public TextView dateTextView;
        @BindView(R.id.eventsRecycleView)
        public RecyclerView eventsRecycleView;

        public EventCardViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if(onDateClickedListener != null) {
                onDateClickedListener.onClicked(groupedEvents.get(position).get(0).getStartDate());
            }
        }
    }

    public interface OnClickedListener {
        void onClicked(Date date);
    }
}
