package pl.edu.pw.mini.grastudio.reminder.utils;


import java.util.Date;

public interface IDateService {
    Date Now();
    boolean isToday(Date date);
}
